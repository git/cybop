/*
 * Copyright (C) 1999-2023. Christian Heller.
 *
 * This file is part of the Cybernetics Oriented Interpreter (CYBOI).
 *
 * CYBOI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * CYBOI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CYBOI. If not, see <http://www.gnu.org/licenses/>.
 *
 * Cybernetics Oriented Programming (CYBOP) <http://www.cybop.org/>
 * CYBOP Developers <cybop-developers@nongnu.org>
 *
 * @version CYBOP 0.27.0 2023-08-31
 * @author Christian Heller <christian.heller@cybop.org>
 */

//
// The C header file "sys/socket.h" does not actually define
// those constants, but instead includes "bits/socket.h".
// This file defines around 38 AF_ constants and
// 38 PF_ constants like this:
//
// #define PF_INET     2   /* IP protocol family.  */
// #define AF_INET     PF_INET
//
// https://stackoverflow.com/questions/2549461/what-is-the-difference-between-af-inet-and-pf-inet-constants
//

//
// Library interface
//

#include "variable.h"

/**
 * Initialises protocol family socket symbolic name
 * (pre-processor-defined) global variables.
 */
void globalise_symbolic_name_socket_protocol_family() {

#if defined(__linux__) || defined(__unix__)
    *UNSPEC_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *UNSPEC_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    //
    // CAUTION! An ALTERNATIVE name for AF_LOCAL with the same ID 1 would be:
    // *UNIX_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *UNIX_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    //
    *LOCAL_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *LOCAL_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *INET_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *INET_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *AX25_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *AX25_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *IPX_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *IPX_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *APPLETALK_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *APPLETALK_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *NETROM_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *NETROM_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *BRIDGE_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *BRIDGE_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *ATMPVC_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *ATMPVC_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *X25_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *X25_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *INET6_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *INET6_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *ROSE_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *ROSE_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *DECNET_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *DECNET_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *NETBEUI_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *NETBEUI_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *SECURITY_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *SECURITY_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *KEY_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *KEY_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    //
    // CAUTION! An ALTERNATIVE name for AF_NETLINK with the same ID 16 would be:
    // *ROUTE_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *ROUTE_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    //
    *NETLINK_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *NETLINK_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *PACKET_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *PACKET_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *ASH_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *ASH_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *ECONET_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *ECONET_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *ATMSVC_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *ATMSVC_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *RDS_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *RDS_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *SNA_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *SNA_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *IRDA_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *IRDA_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *PPPOX_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *PPPOX_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *WANPIPE_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *WANPIPE_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *LLC_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *LLC_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *CAN_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *CAN_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *TIPC_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *TIPC_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *BLUETOOTH_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *BLUETOOTH_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *IUCV_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *IUCV_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *RXRPC_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *RXRPC_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *ISDN_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *ISDN_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *PHONET_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *PHONET_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *IEEE802154_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *IEEE802154_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *CAIF_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *CAIF_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *ALG_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *ALG_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *NFC_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *NFC_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
#elif defined(__APPLE__) && defined(__MACH__)
    *UNSPEC_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *UNSPEC_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    //
    // CAUTION! An ALTERNATIVE name for AF_LOCAL with the same ID 1 would be:
    // *UNIX_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *UNIX_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    //
    *LOCAL_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *LOCAL_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *INET_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *INET_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *AX25_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *AX25_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *IPX_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *IPX_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *APPLETALK_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *APPLETALK_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *NETROM_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *NETROM_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *BRIDGE_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *BRIDGE_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *ATMPVC_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *ATMPVC_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *X25_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *X25_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *INET6_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *INET6_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *ROSE_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *ROSE_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *DECNET_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *DECNET_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *NETBEUI_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *NETBEUI_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *SECURITY_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *SECURITY_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *KEY_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *KEY_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    //
    // CAUTION! An ALTERNATIVE name for AF_NETLINK with the same ID 16 would be:
    // *ROUTE_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *ROUTE_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    //
    *NETLINK_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *NETLINK_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *PACKET_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *PACKET_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *ASH_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *ASH_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *ECONET_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *ECONET_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *ATMSVC_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *ATMSVC_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *RDS_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *RDS_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *SNA_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *SNA_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *IRDA_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *IRDA_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *PPPOX_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *PPPOX_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *WANPIPE_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *WANPIPE_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *LLC_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *LLC_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *CAN_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *CAN_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *TIPC_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *TIPC_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *BLUETOOTH_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *BLUETOOTH_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *IUCV_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *IUCV_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *RXRPC_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *RXRPC_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *ISDN_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *ISDN_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *PHONET_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *PHONET_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *IEEE802154_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *IEEE802154_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *CAIF_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *CAIF_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *ALG_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *ALG_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *NFC_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *NFC_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
// Use __CYGWIN__ too, if _WIN32 is not known to mingw.
#elif defined(_WIN32) || defined(__CYGWIN__)
    *UNSPEC_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *UNSPEC_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    //
    // CAUTION! The local unix domain socket AF_LOCAL or AF_UNIX
    // with ID 1 does NOT exist in windows operating system.
    //
    *INET_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *INET_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *IPX_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *IPX_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *APPLETALK_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *APPLETALK_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *NETBIOS_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *NETBIOS_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *INET6_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *INET6_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *IRDA_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *IRDA_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
    *BLUETOOTH_PROTOCOL_FAMILY_SOCKET_SYMBOLIC_NAME = *BLUETOOTH_ADDRESS_FAMILY_SOCKET_SYMBOLIC_NAME;
#else
    #error "Could not compile system. The operating system is not supported. Check out defined preprocessor macros!"
#endif
}
