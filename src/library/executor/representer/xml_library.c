/*
 * Copyright (C) 1999-2023. Christian Heller.
 *
 * This file is part of the Cybernetics Oriented Interpreter (CYBOI).
 *
 * CYBOI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * CYBOI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CYBOI. If not, see <http://www.gnu.org/licenses/>.
 *
 * Cybernetics Oriented Programming (CYBOP) <http://www.cybop.org/>
 * CYBOP Developers <cybop-developers@nongnu.org>
 *
 * @version CYBOP 0.27.0 2023-08-31
 * @author Christian Heller <christian.heller@cybop.org>
 */

//
// dtd
//

//?? TODO

//
// xml
//

#include "../../../executor/representer/deserialiser/xml/attribute_name_xml_deserialiser.c"
#include "../../../executor/representer/deserialiser/xml/attribute_value_xml_deserialiser.c"
#include "../../../executor/representer/deserialiser/xml/attribute_xml_deserialiser.c"
#include "../../../executor/representer/deserialiser/xml/comment_xml_deserialiser.c"
#include "../../../executor/representer/deserialiser/xml/compound_check_xml_deserialiser.c"
#include "../../../executor/representer/deserialiser/xml/constraints_xml_deserialiser.c"
#include "../../../executor/representer/deserialiser/xml/content_check_xml_deserialiser.c"
#include "../../../executor/representer/deserialiser/xml/content_xml_deserialiser.c"
#include "../../../executor/representer/deserialiser/xml/declaration_xml_deserialiser.c"
#include "../../../executor/representer/deserialiser/xml/definition_xml_deserialiser.c"
#include "../../../executor/representer/deserialiser/xml/element_xml_deserialiser.c"
#include "../../../executor/representer/deserialiser/xml/end_tag_xml_deserialiser.c"
#include "../../../executor/representer/deserialiser/xml/normalisation_xml_deserialiser.c"
#include "../../../executor/representer/deserialiser/xml/string_xml_deserialiser.c"
#include "../../../executor/representer/deserialiser/xml/tag_name_xml_deserialiser.c"
#include "../../../executor/representer/deserialiser/xml/xml_deserialiser.c"

#include "../../../executor/representer/serialiser/xml/attribute_xml_serialiser.c"
#include "../../../executor/representer/serialiser/xml/attributes_xml_serialiser.c"
#include "../../../executor/representer/serialiser/xml/begin_xml_serialiser.c"
#include "../../../executor/representer/serialiser/xml/break_xml_serialiser.c"
#include "../../../executor/representer/serialiser/xml/constraints_xml_serialiser.c"
#include "../../../executor/representer/serialiser/xml/content_xml_serialiser.c"
#include "../../../executor/representer/serialiser/xml/element_xml_serialiser.c"
#include "../../../executor/representer/serialiser/xml/empty_xml_serialiser.c"
#include "../../../executor/representer/serialiser/xml/end_xml_serialiser.c"
#include "../../../executor/representer/serialiser/xml/filled_xml_serialiser.c"
#include "../../../executor/representer/serialiser/xml/indentation_xml_serialiser.c"
#include "../../../executor/representer/serialiser/xml/part_xml_serialiser.c"
#include "../../../executor/representer/serialiser/xml/primitive_xml_serialiser.c"
#include "../../../executor/representer/serialiser/xml/xml_serialiser.c"

#include "../../../executor/selector/xml/attribute_begin_or_tag_end_xml_selector.c"
#include "../../../executor/selector/xml/attribute_name_xml_selector.c"
#include "../../../executor/selector/xml/attribute_value_xml_selector.c"
#include "../../../executor/selector/xml/comment_xml_selector.c"
#include "../../../executor/selector/xml/compound_check_xml_selector.c"
#include "../../../executor/selector/xml/content_check_xml_selector.c"
#include "../../../executor/selector/xml/content_xml_selector.c"
#include "../../../executor/selector/xml/declaration_xml_selector.c"
#include "../../../executor/selector/xml/definition_xml_selector.c"
#include "../../../executor/selector/xml/end_tag_xml_selector.c"

//
// xsd
//

//?? TODO
