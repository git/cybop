/*
 * Copyright (C) 1999-2023. Christian Heller.
 *
 * This file is part of the Cybernetics Oriented Interpreter (CYBOI).
 *
 * CYBOI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * CYBOI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CYBOI. If not, see <http://www.gnu.org/licenses/>.
 *
 * Cybernetics Oriented Programming (CYBOP) <http://www.cybop.org/>
 * CYBOP Developers <cybop-developers@nongnu.org>
 *
 * @version CYBOP 0.27.0 2023-08-31
 * @author Christian Heller <christian.heller@cybop.org>
 */

//
// System interface
//

#include <stdio.h> // stdout
#include <wchar.h> // fwprintf

//
// Library interface
//

#include "arithmetic.h"
#include "constant.h"
#include "knowledge.h"
#include "logger.h"

/**
 * Pops (removes) parts from stack memory.
 *
 * @param p0 the stack memory item
 * @param p1 the parts count
 */
void pop(void* p0, void* p1) {

    log_message_terminated((void*) DEBUG_LEVEL_LOG_CYBOI_MODEL, (void*) L"Pop.");
    //?? fwprintf(stdout, L"Debug: Pop. p1: %i\n", p1);
    //
    // CAUTION! The parts count may be null, since runtime arguments exist only
    // for compound logic operations called via "text/cybol-path" but NOT for
    // those being defined and called directly using the format "element/part".
    //
    // Therefore, comment out this test log message.
    //
    //?? fwprintf(stdout, L"Debug: Pop. *p1: %i\n", *((int*) p1));
    //

    //
    // Declaration
    //

    // The stack memory item data, count.
    void* smd = *NULL_POINTER_STATE_CYBOI_MODEL;
    void* smc = *NULL_POINTER_STATE_CYBOI_MODEL;
    // The first element index.
    int f = *NUMBER_0_INTEGER_STATE_CYBOI_MODEL;
    // The loop variable.
    int j = *NUMBER_0_INTEGER_STATE_CYBOI_MODEL;
    // The break flag.
    int b = *FALSE_BOOLEAN_STATE_CYBOI_MODEL;

    //
    // Initialisation
    //

    // Get stack memory item data, count.
    copy_array_forward((void*) &smd, p0, (void*) POINTER_STATE_CYBOI_TYPE, (void*) FALSE_BOOLEAN_STATE_CYBOI_MODEL, (void*) PRIMITIVE_STATE_CYBOI_MODEL_COUNT, (void*) VALUE_PRIMITIVE_STATE_CYBOI_NAME, (void*) DATA_ITEM_STATE_CYBOI_NAME);
    copy_array_forward((void*) &smc, p0, (void*) POINTER_STATE_CYBOI_TYPE, (void*) FALSE_BOOLEAN_STATE_CYBOI_MODEL, (void*) PRIMITIVE_STATE_CYBOI_MODEL_COUNT, (void*) VALUE_PRIMITIVE_STATE_CYBOI_NAME, (void*) COUNT_ITEM_STATE_CYBOI_NAME);

    // Initialise first element index with stack memory count.
    copy_integer((void*) &f, smc);
    //
    // Subtract parts count from first element index.
    //
    // CAUTION! The parts count may be null, since runtime arguments exist only
    // for compound logic operations called via "text/cybol-path" but NOT for
    // those being defined and called directly using the format "element/part".
    //
    // However, this is NOT a problem, since a null value is just IGNORED here.
    //
    calculate_integer_subtract((void*) &f, p1);
    // CAUTION! Do NOT subtract one here, since the index is already correct.

    // Initialise loop variable with stack memory count.
    copy_integer((void*) &j, smc);
    // Subtract one from loop variable, since this is an INDEX.
    calculate_integer_subtract((void*) &j, (void*) NUMBER_1_INTEGER_STATE_CYBOI_MODEL);

    //
    // Iteration
    //

    //
    // CAUTION! This loop is running BACKWARDS for more efficiency.
    // It is much faster to remove an element from the end of an array,
    // since when removing from somewhere in between, all following
    // elements have to be moved one position forward.
    //

    while (*TRUE_BOOLEAN_STATE_CYBOI_MODEL) {

        compare_integer_less((void*) &b, (void*) &j, (void*) &f);

        if (b != *FALSE_BOOLEAN_STATE_CYBOI_MODEL) {

            break;

        } else {

            // Pop (remove) part from stack memory.
            pop_part(p0, smd, (void*) &j);

            // Decrement loop variable.
            j--;
        }
    }
}
