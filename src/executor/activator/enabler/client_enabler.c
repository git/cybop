/*
 * Copyright (C) 1999-2023. Christian Heller.
 *
 * This file is part of the Cybernetics Oriented Interpreter (CYBOI).
 *
 * CYBOI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * CYBOI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CYBOI. If not, see <http://www.gnu.org/licenses/>.
 *
 * Cybernetics Oriented Programming (CYBOP) <http://www.cybop.org/>
 * CYBOP Developers <cybop-developers@nongnu.org>
 *
 * @version CYBOP 0.27.0 2023-08-31
 * @author Christian Heller <christian.heller@cybop.org>
 */

//
// System interface
//

#include <stdio.h> // stdout
#include <wchar.h> // fwprintf

//
// Library interface
//

#include "arithmetic.h"
#include "constant.h"
#include "logger.h"
#include "server.h"

/**
 * Determine sender client identification.
 *
 * @param p0 the destination sender client identification (socket number)
 * @param p1 the server entry
 * @param p2 the channel
 */
void enable_client(void* p0, void* p1, void* p2) {

    log_message_terminated((void*) DEBUG_LEVEL_LOG_CYBOI_MODEL, (void*) L"Enable client.");
    //?? fwprintf(stdout, L"Debug: Enable client. channel p2: %i\n", p2);
    //?? fwprintf(stdout, L"Debug: Enable client. channel *p2: %i\n", *((int*) p2));

    // The comparison result.
    int r = *FALSE_BOOLEAN_STATE_CYBOI_MODEL;

    if (r == *FALSE_BOOLEAN_STATE_CYBOI_MODEL) {

        compare_integer_equal((void*) &r, p2, (void*) DISPLAY_CYBOI_CHANNEL);

        if (r != *FALSE_BOOLEAN_STATE_CYBOI_MODEL) {

            //
            // CAUTION! The destination sender client identification
            // is NOT forwarded here ON PURPOSE.
            //
            // The window clients have to get created MANUALLY in
            // the corresponding cybol application, so that returning
            // a window identification does NOT make sense here.
            //
            // Furthermore, the calling function "enable_request" stores the
            // returned client identifications in a request input buffer item,
            // and also informs the interrupt pipe via handler,
            // which is NOT wanted HERE and would only cause errors.
            //
            enable_display(p1);
        }
    }

    if (r == *FALSE_BOOLEAN_STATE_CYBOI_MODEL) {

        compare_integer_equal((void*) &r, p2, (void*) SOCKET_CYBOI_CHANNEL);

        if (r != *FALSE_BOOLEAN_STATE_CYBOI_MODEL) {

            enable_socket(p0, p1);
        }
    }

    if (r == *FALSE_BOOLEAN_STATE_CYBOI_MODEL) {

        log_message_terminated((void*) WARNING_LEVEL_LOG_CYBOI_MODEL, (void*) L"Could not enable client. The channel is unknown.");
        fwprintf(stdout, L"Warning: Could not enable client. The channel is unknown. Channel p2: %i\n", p2);
        fwprintf(stdout, L"Warning: Could not enable client. The channel is unknown. Channel *p2: %i\n", *((int*) p2));
    }
}
