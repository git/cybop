/*
 * Copyright (C) 1999-2023. Christian Heller.
 *
 * This file is part of the Cybernetics Oriented Interpreter (CYBOI).
 *
 * CYBOI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * CYBOI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CYBOI. If not, see <http://www.gnu.org/licenses/>.
 *
 * Cybernetics Oriented Programming (CYBOP) <http://www.cybop.org/>
 * CYBOP Developers <cybop-developers@nongnu.org>
 *
 * @version CYBOP 0.27.0 2023-08-31
 * @author Christian Heller <christian.heller@cybop.org>
 */

//
// Library interface
//

#include "arithmetic.h"
#include "communication.h"
#include "constant.h"
#include "knowledge.h"
#include "logger.h"

/**
 * Deserialises the source year/month/day into the destination julian day.
 *
 * http://de.wikipedia.org/wiki/Umrechnung_zwischen_Julianischem_Datum_und_Gregorianischem_Kalender
 *
 * @param p0 the destination julian day integer
 * @param p1 the source year integer
 * @param p2 the source month integer
 * @param p3 the source day integer
 */
void deserialise_time_scale_gregorian_calendar_julian_day(void* p0, void* p1, void* p2, void* p3) {

    log_message_terminated((void*) DEBUG_LEVEL_LOG_CYBOI_MODEL, (void*) L"Deserialise time scale gregorian calendar julian day.");

    // The running day.
    int rd = *NUMBER_0_INTEGER_STATE_CYBOI_MODEL;
    // The running year.
    int ry = *NUMBER_0_INTEGER_STATE_CYBOI_MODEL;
    // The full 400-year-cycles since the first year.
    int n400 = *NUMBER_0_INTEGER_STATE_CYBOI_MODEL;
    // The number of full 100-year-cycles of the last 400-year-cycle.
    int n100 = *NUMBER_0_INTEGER_STATE_CYBOI_MODEL;
    // The number of full 4-year-cycles of the last 100-year-cycle.
    int n4 = *NUMBER_0_INTEGER_STATE_CYBOI_MODEL;
    // The number of full years in the last, incomplete 4-year-cycle.
    int n1 = *NUMBER_0_INTEGER_STATE_CYBOI_MODEL;
    // The full years in the last, incomplete 400-year-cycle.
    int r400 = *NUMBER_0_INTEGER_STATE_CYBOI_MODEL;
    // The number of full years in the last, incomplete 100-year-cycle.
    int r100 = *NUMBER_0_INTEGER_STATE_CYBOI_MODEL;
    // The overall 400-year-cycles.
    int o400 = *NUMBER_0_INTEGER_STATE_CYBOI_MODEL;
    // The overall 100-year-cycles.
    int o100 = *NUMBER_0_INTEGER_STATE_CYBOI_MODEL;
    // The overall 4-year-cycles.
    int o4 = *NUMBER_0_INTEGER_STATE_CYBOI_MODEL;
    // The overall years.
    int o1 = *NUMBER_0_INTEGER_STATE_CYBOI_MODEL;

    // Calculate running day.
    deserialise_time_scale_running_day((void*) &rd, p1, p2, p3);

    // Initialise running year.
    copy_integer((void*) &ry, p1);

    // Calculate running year.
    calculate_integer_subtract((void*) &ry, (void*) NUMBER_1_INTEGER_STATE_CYBOI_MODEL);

    // Initialise number of full 400-year-cycles since the first year.
    copy_integer((void*) &n400, (void*) &ry);
    // Initialise number of full years in the last, incomplete 400-year-cycle.
    copy_integer((void*) &r400, (void*) &ry);

    // Calculate number of full 400-year-cycles since the first year.
    calculate_integer_divide((void*) &n400, (void*) CYCLE_400_YEAR_CALENDAR_TIME_SCALE_MODEL);
    // Calculate number of full years in the last, incomplete 400-year-cycle.
    calculate_integer_modulo((void*) &r400, (void*) CYCLE_400_YEAR_CALENDAR_TIME_SCALE_MODEL);

    // Initialise number of full 100-year-cycles of the last 400-year-cycle.
    copy_integer((void*) &n100, (void*) &r400);
    // Initialise number of full years in the last, incomplete 100-year-cycle.
    copy_integer((void*) &r100, (void*) &r400);

    // Calculate number of full 100-year-cycles of the last 400-year-cycle.
    calculate_integer_divide((void*) &n100, (void*) CYCLE_100_YEAR_CALENDAR_TIME_SCALE_MODEL);
    // Calculate number of full years in the last, incomplete 100-year-cycle.
    calculate_integer_modulo((void*) &r100, (void*) CYCLE_100_YEAR_CALENDAR_TIME_SCALE_MODEL);

    // Initialise number of full 4-year-cycles of the last 100-year-cycle.
    copy_integer((void*) &n4, (void*) &r100);
    // Initialise number of full years in the last, incomplete 4-year-cycle.
    copy_integer((void*) &n1, (void*) &r100);

    // Calculate number of full 4-year-cycles of the last 100-year-cycle.
    calculate_integer_divide((void*) &n4, (void*) CYCLE_4_YEAR_CALENDAR_TIME_SCALE_MODEL);
    // Calculate number of full years in the last, incomplete 4-year-cycle.
    calculate_integer_modulo((void*) &n1, (void*) CYCLE_4_YEAR_CALENDAR_TIME_SCALE_MODEL);

    // Initialise full cycles.
    copy_integer((void*) &o400, (void*) &n400);
    copy_integer((void*) &o100, (void*) &n100);
    copy_integer((void*) &o4, (void*) &n4);
    copy_integer((void*) &o1, (void*) &n1);

    // Calculate full cycles.
    calculate_integer_multiply((void*) &o400, (void*) CYCLE_400_DAY_CALENDAR_TIME_SCALE_MODEL);
    calculate_integer_multiply((void*) &o100, (void*) CYCLE_100_DAY_CALENDAR_TIME_SCALE_MODEL);
    calculate_integer_multiply((void*) &o4, (void*) CYCLE_4_DAY_CALENDAR_TIME_SCALE_MODEL);
    calculate_integer_multiply((void*) &o1, (void*) CYCLE_1_DAY_CALENDAR_TIME_SCALE_MODEL);

    // Initialise julian day.
    copy_integer(p0, (void*) DAY_0_JULIAN_DATE_TIME_SCALE_MODEL);

    // Calculate julian day.
    calculate_integer_add(p0, (void*) &o400);
    calculate_integer_add(p0, (void*) &o100);
    calculate_integer_add(p0, (void*) &o4);
    calculate_integer_add(p0, (void*) &o1);
    calculate_integer_add(p0, (void*) &rd);
}
