/*
 * Copyright (C) 1999-2023. Christian Heller.
 *
 * This file is part of the Cybernetics Oriented Interpreter (CYBOI).
 *
 * CYBOI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * CYBOI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CYBOI. If not, see <http://www.gnu.org/licenses/>.
 *
 * Cybernetics Oriented Programming (CYBOP) <http://www.cybop.org/>
 * CYBOP Developers <cybop-developers@nongnu.org>
 *
 * @version CYBOP 0.27.0 2023-08-31
 * @author Christian Heller <christian.heller@cybop.org>
 */

//
// System interface
//

#include <stdio.h> // stdout
#include <wchar.h> // fwprintf

//
// Library interface
//

#include "arithmetic.h"
#include "communication.h"
#include "constant.h"
#include "knowledge.h"
#include "logger.h"
#include "type.h"

/**
 * Deserialises the numeral post point value.
 *
 * It is also called decimal places (decimals), which is not
 * quite correct, since other number bases than ten may be used.
 *
 * @param p0 the destination post point value
 * @param p1 the source data position (pointer reference)
 * @param p2 the source count remaining
 * @param p3 the number base
 * @param p4 the decimal power flag
 * @param p5 the detected format
 * @param p6 the detected type
 */
void deserialise_numeral_decimals(void* p0, void* p1, void* p2, void* p3, void* p4, void* p5, void* p6) {

    log_message_terminated((void*) DEBUG_LEVEL_LOG_CYBOI_MODEL, (void*) L"Deserialise numeral decimals.");
    //?? fwprintf(stdout, L"Debug: Deserialise numeral decimals. source count remaining p2: %i\n", p2);
    //?? fwprintf(stdout, L"Debug: Deserialise numeral decimals. source count remaining *p2: %i\n", *((int*) p2));

    // The break flag.
    int b = *FALSE_BOOLEAN_STATE_CYBOI_MODEL;
    // The decimals (post point value) data, count.
    void* dd = *NULL_POINTER_STATE_CYBOI_MODEL;
    int dc = *NUMBER_0_INTEGER_STATE_CYBOI_MODEL;

    // Initialise decimals (post point value) data.
    copy_pointer((void*) &dd, p1);

    if (p2 == *NULL_POINTER_STATE_CYBOI_MODEL) {

        //
        // CAUTION! If the loop count handed over as parametre is NULL,
        // then the break flag will NEVER be set to true, because the loop
        // variable comparison does (correctly) not consider null values.
        // Therefore, in this case, the break flag is set to true already here.
        //
        // Initialising the break flag with true will NOT work either, since it:
        // a) will be left untouched if a comparison operand is null;
        // b) would have to be reset to true in each loop cycle.
        //
        copy_integer((void*) &b, (void*) TRUE_BOOLEAN_STATE_CYBOI_MODEL);
    }

    while (*TRUE_BOOLEAN_STATE_CYBOI_MODEL) {

        compare_integer_less_or_equal((void*) &b, p2, (void*) NUMBER_0_INTEGER_STATE_CYBOI_MODEL);

        if (b != *FALSE_BOOLEAN_STATE_CYBOI_MODEL) {

            //
            // Either an end character was found in the selector
            // OR the source count remaining is zero.
            //
            // In BOTH cases, the decimals (post point value) can now be deserialised.
            //

            //?? fwprintf(stdout, L"Debug: Deserialise numeral decimals. dc: %i\n", dc);
            //?? fwprintf(stdout, L"Debug: Deserialise numeral decimals. dd: %ls\n", (wchar_t*) dd);

            // Deserialise fractional digits.
            deserialise_numeral_fraction(p0, dd, (void*) &dc, p3);

            break;

        } else {

            // Select numeral decimals (post point value).
            select_numeral_value(p1, p2, *NULL_POINTER_STATE_CYBOI_MODEL, *NULL_POINTER_STATE_CYBOI_MODEL, *NULL_POINTER_STATE_CYBOI_MODEL, *NULL_POINTER_STATE_CYBOI_MODEL, p3, *NULL_POINTER_STATE_CYBOI_MODEL, p4, p5, p6, (void*) &dc, (void*) &b);
        }
    }
}
