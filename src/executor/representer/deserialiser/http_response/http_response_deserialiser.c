/*
 * Copyright (C) 1999-2023. Christian Heller.
 *
 * This file is part of the Cybernetics Oriented Interpreter (CYBOI).
 *
 * CYBOI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * CYBOI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CYBOI. If not, see <http://www.gnu.org/licenses/>.
 *
 * Cybernetics Oriented Programming (CYBOP) <http://www.cybop.org/>
 * CYBOP Developers <cybop-developers@nongnu.org>
 *
 * @version CYBOP 0.27.0 2023-08-31
 * @author Christian Heller <christian.heller@cybop.org>
 */

//
// Library interface
//

#include "logger.h"

//
// Many Web servers supply incorrect Content-Type headers with their
// HTTP responses. In order to be compatible with these Web servers,
// Web browsers must consider the content of HTTP responses as well as
// the Content-Type header when determining the effective mime type of
// the response. The following document describes an algorithm for
// determining the effective mime type of HTTP responses that balances
// security and compatibility considerations:
//
// http://tools.ietf.org/html/draft-abarth-mime-sniff-00
//
// CAUTION! This is a draft document that, by the rules of IETF,
// has to be referenced as "work in progress", which is hereby done.
//

/**
 * Deserialises the http response into a model and properties.
 *
 * @param p0 the destination model item
 * @param p1 the destination properties item
 * @param p2 the source data
 * @param p3 the source count
 */
void deserialise_http_response(void* p0, void* p1, void* p2, void* p3) {

    log_message_terminated((void*) INFORMATION_LEVEL_LOG_CYBOI_MODEL, (void*) L"Deserialise http response.");
}
