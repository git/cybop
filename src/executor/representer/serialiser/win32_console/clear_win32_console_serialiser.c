/*
 * Copyright (C) 1999-2023. Christian Heller.
 *
 * This file is part of the Cybernetics Oriented Interpreter (CYBOI).
 *
 * CYBOI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * CYBOI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CYBOI. If not, see <http://www.gnu.org/licenses/>.
 *
 * Cybernetics Oriented Programming (CYBOP) <http://www.cybop.org/>
 * CYBOP Developers <cybop-developers@nongnu.org>
 *
 * @version CYBOP 0.27.0 2023-08-31
 * @author Christian Heller <christian.heller@cybop.org>
 */

//
// System interface
//

#include <windows.h>

//
// Library interface
//

#include "constant.h"
#include "logger.h"

/**
 * Clears the terminal screen using win32 console function calls.
 *
 * @param p0 the destination win32 console output data
 */
void serialise_win32_console_clear(void* p0) {

    if (p0 != *NULL_POINTER_STATE_CYBOI_MODEL) {

        int* d = (int*) p0;

        // Cast DEREFERENCED value to handle.
        // CAUTION! The output data is stored as int value,
        // but actually references a win32 console handle.
        // This is just to be sure that the correct type is used.
        HANDLE dh = (HANDLE) *d;

        log_message_terminated((void*) DEBUG_LEVEL_LOG_CYBOI_MODEL, (void*) L"Serialise win32 console clear.");

        // The cursor home.
        COORD coordScreen = { 0, 0 };
        DWORD cCharsWritten;
        // The console screen buffer info.
        CONSOLE_SCREEN_BUFFER_INFO i;

        // Get console screen buffer info.
        BOOL b = GetConsoleScreenBufferInfo(dh, &i);

        // If the return value is zero, then an error occured.
        if (b != *FALSE_BOOLEAN_STATE_CYBOI_MODEL) {

            // Get number of console character cells.
            DWORD dwConSize = i.dwSize.X * i.dwSize.Y;

            // Fill entire screen with blanks.
            b = FillConsoleOutputCharacter(dh, (TCHAR) ' ', dwConSize, coordScreen, &cCharsWritten);

            // If the return value is zero, then an error occured.
            if (b != *FALSE_BOOLEAN_STATE_CYBOI_MODEL) {

                // Get current text attributes.
                b = GetConsoleScreenBufferInfo(dh, &i);

                // If the return value is zero, then an error occured.
                if (b != *FALSE_BOOLEAN_STATE_CYBOI_MODEL) {

                    // Set console attributes accordingly.
                    b = FillConsoleOutputAttribute(dh, i.wAttributes, dwConSize, coordScreen, &cCharsWritten);

                    // If the return value is zero, then an error occured.
                    if (b != *FALSE_BOOLEAN_STATE_CYBOI_MODEL) {

                        // Position cursor at its home coordinates.
                        b = SetConsoleCursorPosition(dh, coordScreen);

                        // If the return value is zero, then an error occured.
                        if (b == *FALSE_BOOLEAN_STATE_CYBOI_MODEL) {

                            // Get the calling thread's last-error code.
                            DWORD e = GetLastError();

                            log_message_terminated((void*) ERROR_LEVEL_LOG_CYBOI_MODEL, (void*) L"Could not serialise win32 console clear. The cursor could not be positioned.");
                            log_error((void*) &e);
                        }

                    } else {

                        // Get the calling thread's last-error code.
                        DWORD e = GetLastError();

                        log_message_terminated((void*) ERROR_LEVEL_LOG_CYBOI_MODEL, (void*) L"Could not serialise win32 console clear. The console attributes could not be set.");
                        log_error((void*) &e);
                    }

                } else {

                    // Get the calling thread's last-error code.
                    DWORD e = GetLastError();

                    log_message_terminated((void*) ERROR_LEVEL_LOG_CYBOI_MODEL, (void*) L"Could not serialise win32 console clear. The text attributes could not be retrieved.");
                    log_error((void*) &e);
                }

            } else {

                // Get the calling thread's last-error code.
                DWORD e = GetLastError();

                log_message_terminated((void*) ERROR_LEVEL_LOG_CYBOI_MODEL, (void*) L"Could not serialise win32 console clear. The console could not be filled with characters.");
                log_error((void*) &e);
            }

        } else {

            // Get the calling thread's last-error code.
            DWORD e = GetLastError();

            log_message_terminated((void*) ERROR_LEVEL_LOG_CYBOI_MODEL, (void*) L"Could not serialise win32 console clear. The console screen buffer info could not be retrieved.");
            log_error((void*) &e);
        }

    } else {

        log_message_terminated((void*) ERROR_LEVEL_LOG_CYBOI_MODEL, (void*) L"Could not serialise win32 console clear. The standard output is null.");
    }
}
