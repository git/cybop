/*
 * Copyright (C) 1999-2023. Christian Heller.
 *
 * This file is part of the Cybernetics Oriented Interpreter (CYBOI).
 *
 * CYBOI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * CYBOI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CYBOI. If not, see <http://www.gnu.org/licenses/>.
 *
 * Cybernetics Oriented Programming (CYBOP) <http://www.cybop.org/>
 * CYBOP Developers <cybop-developers@nongnu.org>
 *
 * @version CYBOP 0.27.0 2023-08-31
 * @author Christian Heller <christian.heller@cybop.org>
 */

//
// System interface
//

#include <windows.h>

//
// Library interface
//

#include "constant.h"
#include "logger.h"

/**
 * Serialises the win32 display text.
 *
 * @param p0 the win32 device text
 * @param p1 the source model data of type "char"
 * @param p2 the source model count
 * @param p3 the position x
 * @param p4 the position y
 * @param p5 the size width
 * @param p6 the size height
 */
void serialise_win32_display_text(void* p0, void* p1, void* p2, void* p3, void* p4, void* p5, void* p6) {

    if (p0 != *NULL_POINTER_STATE_CYBOI_MODEL) {

        // The handle to the device context.
        //
        // CAUTION! The device context type is defined as:
        // typedef HANDLE HDC;
        // typedef PVOID HANDLE;
        // typedef void* PVOID;
        //
        // The HDC type is: void*
        // Therefore, cast parametre value AS IS
        // to handle (WITHOUT dereferencing).
        HDC h = (HDC) p0;

        log_message_terminated((void*) DEBUG_LEVEL_LOG_CYBOI_MODEL, (void*) L"Serialise win32 display text.");

    } else {

        log_message_terminated((void*) ERROR_LEVEL_LOG_CYBOI_MODEL, (void*) L"Could not serialise win32 display text. The device context is null.");
    }
}
