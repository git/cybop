﻿namespace Cybop.Designer.Formulare
{
    partial class frmMain
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.statusStripMain = new System.Windows.Forms.StatusStrip();
            this.lblRevision = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelStatTextLength = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelStatTextLines = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelStateTextPosition = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStripMain = new System.Windows.Forms.MenuStrip();
            this.dateiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.bearbeitenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.fensterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.navigationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.extrasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.einstellungenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMain = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tcEditor = new System.Windows.Forms.TabControl();
            this.toolStripButtonCut = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonCopy = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonPaste = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonReturn = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonForward = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonKommentarblock = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonSearch = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonFindReplace = new System.Windows.Forms.ToolStripButton();
            this.neuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oeffnenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.speichernToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.speichernUnternToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.beendenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ausschneidenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kopierenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.einfuegenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rueckgaengigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wiederholenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kommentarblockFestlegenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStripMain.SuspendLayout();
            this.menuStripMain.SuspendLayout();
            this.toolStripMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStripMain
            // 
            this.statusStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblRevision,
            this.toolStripStatusLabelStatTextLength,
            this.toolStripStatusLabelStatTextLines,
            this.toolStripStatusLabelStateTextPosition});
            resources.ApplyResources(this.statusStripMain, "statusStripMain");
            this.statusStripMain.Name = "statusStripMain";
            // 
            // lblRevision
            // 
            this.lblRevision.Name = "lblRevision";
            resources.ApplyResources(this.lblRevision, "lblRevision");
            // 
            // toolStripStatusLabelStatTextLength
            // 
            this.toolStripStatusLabelStatTextLength.Name = "toolStripStatusLabelStatTextLength";
            resources.ApplyResources(this.toolStripStatusLabelStatTextLength, "toolStripStatusLabelStatTextLength");
            // 
            // toolStripStatusLabelStatTextLines
            // 
            this.toolStripStatusLabelStatTextLines.Name = "toolStripStatusLabelStatTextLines";
            resources.ApplyResources(this.toolStripStatusLabelStatTextLines, "toolStripStatusLabelStatTextLines");
            // 
            // toolStripStatusLabelStateTextPosition
            // 
            this.toolStripStatusLabelStateTextPosition.Name = "toolStripStatusLabelStateTextPosition";
            resources.ApplyResources(this.toolStripStatusLabelStateTextPosition, "toolStripStatusLabelStateTextPosition");
            // 
            // menuStripMain
            // 
            this.menuStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dateiToolStripMenuItem,
            this.bearbeitenToolStripMenuItem,
            this.fensterToolStripMenuItem,
            this.extrasToolStripMenuItem});
            resources.ApplyResources(this.menuStripMain, "menuStripMain");
            this.menuStripMain.Name = "menuStripMain";
            // 
            // dateiToolStripMenuItem
            // 
            this.dateiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.neuToolStripMenuItem,
            this.oeffnenToolStripMenuItem,
            this.toolStripMenuItem1,
            this.speichernToolStripMenuItem,
            this.speichernUnternToolStripMenuItem,
            this.toolStripMenuItem2,
            this.beendenToolStripMenuItem});
            this.dateiToolStripMenuItem.Name = "dateiToolStripMenuItem";
            resources.ApplyResources(this.dateiToolStripMenuItem, "dateiToolStripMenuItem");
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            resources.ApplyResources(this.toolStripMenuItem1, "toolStripMenuItem1");
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            resources.ApplyResources(this.toolStripMenuItem2, "toolStripMenuItem2");
            // 
            // bearbeitenToolStripMenuItem
            // 
            this.bearbeitenToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ausschneidenToolStripMenuItem,
            this.kopierenToolStripMenuItem,
            this.einfuegenToolStripMenuItem,
            this.toolStripMenuItem3,
            this.rueckgaengigToolStripMenuItem,
            this.wiederholenToolStripMenuItem,
            this.toolStripMenuItem4});
            this.bearbeitenToolStripMenuItem.Name = "bearbeitenToolStripMenuItem";
            resources.ApplyResources(this.bearbeitenToolStripMenuItem, "bearbeitenToolStripMenuItem");
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            resources.ApplyResources(this.toolStripMenuItem3, "toolStripMenuItem3");
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            resources.ApplyResources(this.toolStripMenuItem4, "toolStripMenuItem4");
            // 
            // fensterToolStripMenuItem
            // 
            this.fensterToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.navigationToolStripMenuItem});
            this.fensterToolStripMenuItem.Name = "fensterToolStripMenuItem";
            resources.ApplyResources(this.fensterToolStripMenuItem, "fensterToolStripMenuItem");
            this.fensterToolStripMenuItem.Click += new System.EventHandler(this.fensterToolStripMenuItem_Click);
            // 
            // navigationToolStripMenuItem
            // 
            this.navigationToolStripMenuItem.Name = "navigationToolStripMenuItem";
            resources.ApplyResources(this.navigationToolStripMenuItem, "navigationToolStripMenuItem");
            this.navigationToolStripMenuItem.Click += new System.EventHandler(this.navigationToolStripMenuItem_Click);
            // 
            // extrasToolStripMenuItem
            // 
            this.extrasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.kommentarblockFestlegenToolStripMenuItem,
            this.einstellungenToolStripMenuItem});
            this.extrasToolStripMenuItem.Name = "extrasToolStripMenuItem";
            resources.ApplyResources(this.extrasToolStripMenuItem, "extrasToolStripMenuItem");
            // 
            // einstellungenToolStripMenuItem
            // 
            this.einstellungenToolStripMenuItem.Image = global::Cybop.Designer.Properties.Resources.Settings_32;
            this.einstellungenToolStripMenuItem.Name = "einstellungenToolStripMenuItem";
            resources.ApplyResources(this.einstellungenToolStripMenuItem, "einstellungenToolStripMenuItem");
            this.einstellungenToolStripMenuItem.Click += new System.EventHandler(this.einstellungenToolStripMenuItem_Click);
            // 
            // toolStripMain
            // 
            this.toolStripMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripMain.ImageScalingSize = new System.Drawing.Size(22, 22);
            this.toolStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator3,
            this.toolStripButtonCut,
            this.toolStripButtonCopy,
            this.toolStripButtonPaste,
            this.toolStripSeparator2,
            this.toolStripButtonReturn,
            this.toolStripButtonForward,
            this.toolStripSeparator1,
            this.toolStripButtonKommentarblock,
            this.toolStripSeparator4,
            this.toolStripButtonSearch,
            this.toolStripButtonFindReplace});
            resources.ApplyResources(this.toolStripMain, "toolStripMain");
            this.toolStripMain.Name = "toolStripMain";
            this.toolStripMain.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            resources.ApplyResources(this.toolStripSeparator3, "toolStripSeparator3");
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            resources.ApplyResources(this.toolStripSeparator2, "toolStripSeparator2");
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            resources.ApplyResources(this.toolStripSeparator1, "toolStripSeparator1");
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            resources.ApplyResources(this.toolStripSeparator4, "toolStripSeparator4");
            // 
            // tcEditor
            // 
            resources.ApplyResources(this.tcEditor, "tcEditor");
            this.tcEditor.Name = "tcEditor";
            this.tcEditor.SelectedIndex = 0;
            this.tcEditor.SelectedIndexChanged += new System.EventHandler(this.tcEditor_SelectedIndexChanged);
            this.tcEditor.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tcEditor_MouseClick);
            this.tcEditor.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.tcEditor_MouseDoubleClick);
            // 
            // toolStripButtonCut
            // 
            this.toolStripButtonCut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonCut.Image = global::Cybop.Designer.Properties.Resources.cut_32;
            resources.ApplyResources(this.toolStripButtonCut, "toolStripButtonCut");
            this.toolStripButtonCut.Name = "toolStripButtonCut";
            this.toolStripButtonCut.Click += new System.EventHandler(this.toolStripButtonCut_Click);
            // 
            // toolStripButtonCopy
            // 
            this.toolStripButtonCopy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonCopy.Image = global::Cybop.Designer.Properties.Resources.copy_32;
            resources.ApplyResources(this.toolStripButtonCopy, "toolStripButtonCopy");
            this.toolStripButtonCopy.Name = "toolStripButtonCopy";
            this.toolStripButtonCopy.Click += new System.EventHandler(this.toolStripButtonCopy_Click);
            // 
            // toolStripButtonPaste
            // 
            this.toolStripButtonPaste.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonPaste.Image = global::Cybop.Designer.Properties.Resources.paste_32;
            resources.ApplyResources(this.toolStripButtonPaste, "toolStripButtonPaste");
            this.toolStripButtonPaste.Name = "toolStripButtonPaste";
            this.toolStripButtonPaste.Click += new System.EventHandler(this.toolStripButtonPaste_Click);
            // 
            // toolStripButtonReturn
            // 
            this.toolStripButtonReturn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonReturn.Image = global::Cybop.Designer.Properties.Resources.return_32;
            resources.ApplyResources(this.toolStripButtonReturn, "toolStripButtonReturn");
            this.toolStripButtonReturn.Name = "toolStripButtonReturn";
            this.toolStripButtonReturn.Click += new System.EventHandler(this.toolStripButtonReturn_Click);
            // 
            // toolStripButtonForward
            // 
            this.toolStripButtonForward.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonForward.Image = global::Cybop.Designer.Properties.Resources.forward_32;
            resources.ApplyResources(this.toolStripButtonForward, "toolStripButtonForward");
            this.toolStripButtonForward.Name = "toolStripButtonForward";
            this.toolStripButtonForward.Click += new System.EventHandler(this.toolStripButtonForward_Click);
            // 
            // toolStripButtonKommentarblock
            // 
            this.toolStripButtonKommentarblock.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonKommentarblock.Image = global::Cybop.Designer.Properties.Resources.comment_block_32;
            resources.ApplyResources(this.toolStripButtonKommentarblock, "toolStripButtonKommentarblock");
            this.toolStripButtonKommentarblock.Name = "toolStripButtonKommentarblock";
            this.toolStripButtonKommentarblock.Click += new System.EventHandler(this.toolStripButtonKommentarblock_Click);
            // 
            // toolStripButtonSearch
            // 
            this.toolStripButtonSearch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonSearch.Image = global::Cybop.Designer.Properties.Resources.fin_32;
            resources.ApplyResources(this.toolStripButtonSearch, "toolStripButtonSearch");
            this.toolStripButtonSearch.Name = "toolStripButtonSearch";
            this.toolStripButtonSearch.Click += new System.EventHandler(this.toolStripButtonSearch_Click);
            // 
            // toolStripButtonFindReplace
            // 
            this.toolStripButtonFindReplace.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonFindReplace.Image = global::Cybop.Designer.Properties.Resources.find_replace_32;
            resources.ApplyResources(this.toolStripButtonFindReplace, "toolStripButtonFindReplace");
            this.toolStripButtonFindReplace.Name = "toolStripButtonFindReplace";
            this.toolStripButtonFindReplace.Click += new System.EventHandler(this.toolStripButtonFindReplace_Click);
            // 
            // neuToolStripMenuItem
            // 
            this.neuToolStripMenuItem.Image = global::Cybop.Designer.Properties.Resources.project_new_32;
            this.neuToolStripMenuItem.Name = "neuToolStripMenuItem";
            resources.ApplyResources(this.neuToolStripMenuItem, "neuToolStripMenuItem");
            this.neuToolStripMenuItem.Click += new System.EventHandler(this.neuToolStripMenuItem_Click);
            // 
            // oeffnenToolStripMenuItem
            // 
            this.oeffnenToolStripMenuItem.Image = global::Cybop.Designer.Properties.Resources.project_open_32;
            this.oeffnenToolStripMenuItem.Name = "oeffnenToolStripMenuItem";
            resources.ApplyResources(this.oeffnenToolStripMenuItem, "oeffnenToolStripMenuItem");
            this.oeffnenToolStripMenuItem.Click += new System.EventHandler(this.oeffnenToolStripMenuItem_Click);
            // 
            // speichernToolStripMenuItem
            // 
            this.speichernToolStripMenuItem.Image = global::Cybop.Designer.Properties.Resources.save_32;
            this.speichernToolStripMenuItem.Name = "speichernToolStripMenuItem";
            resources.ApplyResources(this.speichernToolStripMenuItem, "speichernToolStripMenuItem");
            this.speichernToolStripMenuItem.Click += new System.EventHandler(this.speichernToolStripMenuItem_Click);
            // 
            // speichernUnternToolStripMenuItem
            // 
            this.speichernUnternToolStripMenuItem.Image = global::Cybop.Designer.Properties.Resources.save_all_32;
            this.speichernUnternToolStripMenuItem.Name = "speichernUnternToolStripMenuItem";
            resources.ApplyResources(this.speichernUnternToolStripMenuItem, "speichernUnternToolStripMenuItem");
            this.speichernUnternToolStripMenuItem.Click += new System.EventHandler(this.speichernUnternToolStripMenuItem_Click);
            // 
            // beendenToolStripMenuItem
            // 
            this.beendenToolStripMenuItem.Image = global::Cybop.Designer.Properties.Resources.exit_32;
            this.beendenToolStripMenuItem.Name = "beendenToolStripMenuItem";
            resources.ApplyResources(this.beendenToolStripMenuItem, "beendenToolStripMenuItem");
            this.beendenToolStripMenuItem.Click += new System.EventHandler(this.beendenToolStripMenuItem_Click);
            // 
            // ausschneidenToolStripMenuItem
            // 
            this.ausschneidenToolStripMenuItem.Image = global::Cybop.Designer.Properties.Resources.cut_32;
            this.ausschneidenToolStripMenuItem.Name = "ausschneidenToolStripMenuItem";
            resources.ApplyResources(this.ausschneidenToolStripMenuItem, "ausschneidenToolStripMenuItem");
            this.ausschneidenToolStripMenuItem.Click += new System.EventHandler(this.ausschneidenToolStripMenuItem_Click);
            // 
            // kopierenToolStripMenuItem
            // 
            this.kopierenToolStripMenuItem.Image = global::Cybop.Designer.Properties.Resources.copy_32;
            this.kopierenToolStripMenuItem.Name = "kopierenToolStripMenuItem";
            resources.ApplyResources(this.kopierenToolStripMenuItem, "kopierenToolStripMenuItem");
            this.kopierenToolStripMenuItem.Click += new System.EventHandler(this.kopierenToolStripMenuItem_Click);
            // 
            // einfuegenToolStripMenuItem
            // 
            this.einfuegenToolStripMenuItem.Image = global::Cybop.Designer.Properties.Resources.paste_32;
            this.einfuegenToolStripMenuItem.Name = "einfuegenToolStripMenuItem";
            resources.ApplyResources(this.einfuegenToolStripMenuItem, "einfuegenToolStripMenuItem");
            this.einfuegenToolStripMenuItem.Click += new System.EventHandler(this.einfuegenToolStripMenuItem_Click);
            // 
            // rueckgaengigToolStripMenuItem
            // 
            this.rueckgaengigToolStripMenuItem.Image = global::Cybop.Designer.Properties.Resources.return_32;
            this.rueckgaengigToolStripMenuItem.Name = "rueckgaengigToolStripMenuItem";
            resources.ApplyResources(this.rueckgaengigToolStripMenuItem, "rueckgaengigToolStripMenuItem");
            this.rueckgaengigToolStripMenuItem.Click += new System.EventHandler(this.rueckgaengigToolStripMenuItem_Click);
            // 
            // wiederholenToolStripMenuItem
            // 
            this.wiederholenToolStripMenuItem.Image = global::Cybop.Designer.Properties.Resources.forward_32;
            this.wiederholenToolStripMenuItem.Name = "wiederholenToolStripMenuItem";
            resources.ApplyResources(this.wiederholenToolStripMenuItem, "wiederholenToolStripMenuItem");
            this.wiederholenToolStripMenuItem.Click += new System.EventHandler(this.wiederholenToolStripMenuItem_Click);
            // 
            // kommentarblockFestlegenToolStripMenuItem
            // 
            this.kommentarblockFestlegenToolStripMenuItem.Image = global::Cybop.Designer.Properties.Resources.comment_block_32;
            this.kommentarblockFestlegenToolStripMenuItem.Name = "kommentarblockFestlegenToolStripMenuItem";
            resources.ApplyResources(this.kommentarblockFestlegenToolStripMenuItem, "kommentarblockFestlegenToolStripMenuItem");
            this.kommentarblockFestlegenToolStripMenuItem.Click += new System.EventHandler(this.kommentarblockFestlegenToolStripMenuItem_Click);
            // 
            // frmMain
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tcEditor);
            this.Controls.Add(this.toolStripMain);
            this.Controls.Add(this.statusStripMain);
            this.Controls.Add(this.menuStripMain);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStripMain;
            this.Name = "frmMain";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.statusStripMain.ResumeLayout(false);
            this.statusStripMain.PerformLayout();
            this.menuStripMain.ResumeLayout(false);
            this.menuStripMain.PerformLayout();
            this.toolStripMain.ResumeLayout(false);
            this.toolStripMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStripMain;
        private System.Windows.Forms.ToolStripStatusLabel lblRevision;
        private System.Windows.Forms.MenuStrip menuStripMain;
        private System.Windows.Forms.ToolStripMenuItem dateiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem neuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem speichernToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem beendenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem speichernUnternToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oeffnenToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStrip toolStripMain;
        private System.Windows.Forms.ToolStripButton toolStripButtonCopy;
        private System.Windows.Forms.ToolStripButton toolStripButtonPaste;
        private System.Windows.Forms.ToolStripMenuItem bearbeitenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kopierenToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButtonReturn;
        private System.Windows.Forms.ToolStripButton toolStripButtonForward;
        private System.Windows.Forms.ToolStripMenuItem einfuegenToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem rueckgaengigToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem wiederholenToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem fensterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem navigationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem extrasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kommentarblockFestlegenToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButtonKommentarblock;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem einstellungenToolStripMenuItem;
        private System.Windows.Forms.TabControl tcEditor;
        private System.Windows.Forms.ToolStripButton toolStripButtonCut;
        private System.Windows.Forms.ToolStripMenuItem ausschneidenToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelStatTextLength;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelStatTextLines;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelStateTextPosition;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton toolStripButtonSearch;
        private System.Windows.Forms.ToolStripButton toolStripButtonFindReplace;
    }
}

