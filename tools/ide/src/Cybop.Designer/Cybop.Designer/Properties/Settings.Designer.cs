﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Dieser Code wurde von einem Tool generiert.
//     Laufzeitversion:4.0.30319.269
//
//     Änderungen an dieser Datei können falsches Verhalten verursachen und gehen verloren, wenn
//     der Code erneut generiert wird.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Cybop.Designer.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "10.0.0.0")]
    public sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("100")]
        public int Transparency {
            get {
                return ((int)(this["Transparency"]));
            }
            set {
                this["Transparency"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string CommentBlock {
            get {
                return ((string)(this["CommentBlock"]));
            }
            set {
                this["CommentBlock"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("White")]
        public global::System.Drawing.Color Hintergrundfarbe {
            get {
                return ((global::System.Drawing.Color)(this["Hintergrundfarbe"]));
            }
            set {
                this["Hintergrundfarbe"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Black")]
        public global::System.Drawing.Color Schriftfarbe {
            get {
                return ((global::System.Drawing.Color)(this["Schriftfarbe"]));
            }
            set {
                this["Schriftfarbe"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Blue")]
        public global::System.Drawing.Color XmlBrackets {
            get {
                return ((global::System.Drawing.Color)(this["XmlBrackets"]));
            }
            set {
                this["XmlBrackets"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Blue")]
        public global::System.Drawing.Color XmlAttributeValue {
            get {
                return ((global::System.Drawing.Color)(this["XmlAttributeValue"]));
            }
            set {
                this["XmlAttributeValue"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Red")]
        public global::System.Drawing.Color XmlAttributeKey {
            get {
                return ((global::System.Drawing.Color)(this["XmlAttributeKey"]));
            }
            set {
                this["XmlAttributeKey"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Black")]
        public global::System.Drawing.Color XmlAttributeMarkup {
            get {
                return ((global::System.Drawing.Color)(this["XmlAttributeMarkup"]));
            }
            set {
                this["XmlAttributeMarkup"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Black")]
        public global::System.Drawing.Color XmlTagContent {
            get {
                return ((global::System.Drawing.Color)(this["XmlTagContent"]));
            }
            set {
                this["XmlTagContent"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Arial Narrow, 8.25pt")]
        public global::System.Drawing.Font Schriftart {
            get {
                return ((global::System.Drawing.Font)(this["Schriftart"]));
            }
            set {
                this["Schriftart"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0, 192, 0")]
        public global::System.Drawing.Color XmlKommentar {
            get {
                return ((global::System.Drawing.Color)(this["XmlKommentar"]));
            }
            set {
                this["XmlKommentar"] = value;
            }
        }
    }
}
