package cybol_eclipse_plugin;

public final class Debug {
  
  public static final boolean SHOW_XML_TOKENS = false;
  public static final boolean SHOW_XML_TREE = false;
  
  
  private Debug() { }
}