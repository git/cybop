package cybol_eclipse_plugin.editor;

public class InvalidXMLDocumentException extends Exception {
  
  private static final long serialVersionUID = 8440201609794497343L;
  
  
  public InvalidXMLDocumentException(String message) {
    
    super(message);
  }
}