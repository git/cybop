/*
 * Copyright (C) 1999-2023. Christian Heller.
 *
 * This file is part of the Cybernetics Oriented Interpreter (CYBOI).
 *
 * CYBOI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * CYBOI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CYBOI. If not, see <http://www.gnu.org/licenses/>.
 *
 * Cybernetics Oriented Programming (CYBOP) <http://www.cybop.org/>
 * CYBOP Developers <cybop-developers@nongnu.org>
 *
 * @version CYBOP 0.27.0 2023-08-31
 * @author Christian Heller <christian.heller@cybop.org>
 */

#ifndef HOSTNAME_UNIX_COMMAND_OPTION_NAME_CONSTANT_HEADER
#define HOSTNAME_UNIX_COMMAND_OPTION_NAME_CONSTANT_HEADER

//
// System interface
//

#include <stddef.h> // wchar_t

//
// Library interface
//

#include "constant.h"

/** The dns hostname unix command option name. */
static wchar_t* DNS_HOSTNAME_UNIX_COMMAND_OPTION_NAME = L"-d";
static int* DNS_HOSTNAME_UNIX_COMMAND_OPTION_NAME_COUNT = NUMBER_2_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The fqdn hostname unix command option name. */
static wchar_t* FQDN_HOSTNAME_UNIX_COMMAND_OPTION_NAME = L"-f";
static int* FQDN_HOSTNAME_UNIX_COMMAND_OPTION_NAME_COUNT = NUMBER_2_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The ipadress hostname unix command option name. */
static wchar_t* IP_HOSTNAME_UNIX_COMMAND_OPTION_NAME = L"-i";
static int* IP_HOSTNAME_UNIX_COMMAND_OPTION_NAME_COUNT = NUMBER_2_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The all ipadresses hostname unix command option name. */
static wchar_t* ALLIP_HOSTNAME_UNIX_COMMAND_OPTION_NAME = L"-I";
static int* ALLIP_HOSTNAME_UNIX_COMMAND_OPTION_NAME_COUNT = NUMBER_2_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The short hostname unix command option name. */
static wchar_t* SHORT_HOSTNAME_UNIX_COMMAND_OPTION_NAME = L"-s";
static int* SHORT_HOSTNAME_UNIX_COMMAND_OPTION_NAME_COUNT = NUMBER_2_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/* HOSTNAME_UNIX_COMMAND_OPTION_NAME_CONSTANT_HEADER */
#endif
