/*
 * Copyright (C) 1999-2023. Christian Heller.
 *
 * This file is part of the Cybernetics Oriented Interpreter (CYBOI).
 *
 * CYBOI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * CYBOI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CYBOI. If not, see <http://www.gnu.org/licenses/>.
 *
 * Cybernetics Oriented Programming (CYBOP) <http://www.cybop.org/>
 * CYBOP Developers <cybop-developers@nongnu.org>
 *
 * @version CYBOP 0.27.0 2023-08-31
 * @author Christian Heller <christian.heller@cybop.org>
 */

#ifndef LIST_OPEN_FILES_UNIX_COMMAND_OPTION_NAME_CONSTANT_HEADER
#define LIST_OPEN_FILES_UNIX_COMMAND_OPTION_NAME_CONSTANT_HEADER

//
// System interface
//

#include <stddef.h> // wchar_t

//
// Library interface
//

#include "constant.h"

/** The list UID numbers list open files unix command option name. */
static wchar_t* LIST_UID_LIST_OPEN_FILES_UNIX_COMMAND_OPTION_NAME = L"-l";
static int* LIST_UID_LIST_OPEN_FILES_UNIX_COMMAND_OPTION_NAME_COUNT = NUMBER_2_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The list file size list open files unix command option name. */
static wchar_t* LIST_FILE_SIZE_LIST_OPEN_FILES_UNIX_COMMAND_OPTION_NAME = L"-s";
static int* LIST_FILE_SIZE_LIST_OPEN_FILES_UNIX_COMMAND_OPTION_NAME_COUNT = NUMBER_2_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The list tasks list open files unix command option name. */
static wchar_t* LIST_TASKS_LIST_OPEN_FILES_UNIX_COMMAND_OPTION_NAME = L"-K";
static int* LIST_TASKS_LIST_OPEN_FILES_UNIX_COMMAND_OPTION_NAME_COUNT = NUMBER_2_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The disable tcp list open files unix command option name. */
static wchar_t* DISABLE_TCP_LIST_OPEN_FILES_UNIX_COMMAND_OPTION_NAME = L"-T";
static int* DISABLE_TCP_LIST_OPEN_FILES_UNIX_COMMAND_OPTION_NAME_COUNT = NUMBER_2_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The terse listing list open files unix command option name. */
static wchar_t* TERSE_LISTING_LIST_OPEN_FILES_UNIX_COMMAND_OPTION_NAME = L"-t";
static int* TERSE_LISTING_LIST_OPEN_FILES_UNIX_COMMAND_OPTION_NAME_COUNT = NUMBER_2_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/* LIST_OPEN_FILES_UNIX_COMMAND_OPTION_NAME_CONSTANT_HEADER */
#endif
