/*
 * Copyright (C) 1999-2023. Christian Heller.
 *
 * This file is part of the Cybernetics Oriented Interpreter (CYBOI).
 *
 * CYBOI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * CYBOI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CYBOI. If not, see <http://www.gnu.org/licenses/>.
 *
 * Cybernetics Oriented Programming (CYBOP) <http://www.cybop.org/>
 * CYBOP Developers <cybop-developers@nongnu.org>
 *
 * @version CYBOP 0.27.0 2023-08-31
 * @author Christian Heller <christian.heller@cybop.org>
 */

#ifndef EVENT_GUI_STATE_CYBOL_NAME_CONSTANT_HEADER
#define EVENT_GUI_STATE_CYBOL_NAME_CONSTANT_HEADER

//
// System interface
//

#include <stddef.h> // wchar_t

//
// Library interface
//

#include "constant.h"

/** The button event gui state cybol name. */
static wchar_t* BUTTON_EVENT_GUI_STATE_CYBOL_NAME = L"button";
static int* BUTTON_EVENT_GUI_STATE_CYBOL_NAME_COUNT = NUMBER_6_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The event event gui state cybol name. */
static wchar_t* EVENT_EVENT_GUI_STATE_CYBOL_NAME = L"event";
static int* EVENT_EVENT_GUI_STATE_CYBOL_NAME_COUNT = NUMBER_5_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The exposed_height event gui state cybol name. */
static wchar_t* EXPOSED_HEIGHT_EVENT_GUI_STATE_CYBOL_NAME = L"exposed_height";
static int* EXPOSED_HEIGHT_EVENT_GUI_STATE_CYBOL_NAME_COUNT = NUMBER_14_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The exposed_width event gui state cybol name. */
static wchar_t* EXPOSED_WIDTH_EVENT_GUI_STATE_CYBOL_NAME = L"exposed_width";
static int* EXPOSED_WIDTH_EVENT_GUI_STATE_CYBOL_NAME_COUNT = NUMBER_13_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The exposed_x event gui state cybol name. */
static wchar_t* EXPOSED_X_EVENT_GUI_STATE_CYBOL_NAME = L"exposed_x";
static int* EXPOSED_X_EVENT_GUI_STATE_CYBOL_NAME_COUNT = NUMBER_9_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The exposed_y event gui state cybol name. */
static wchar_t* EXPOSED_Y_EVENT_GUI_STATE_CYBOL_NAME = L"exposed_y";
static int* EXPOSED_Y_EVENT_GUI_STATE_CYBOL_NAME_COUNT = NUMBER_9_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The keycode event gui state cybol name. */
static wchar_t* KEYCODE_EVENT_GUI_STATE_CYBOL_NAME = L"keycode";
static int* KEYCODE_EVENT_GUI_STATE_CYBOL_NAME_COUNT = NUMBER_7_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The mask event gui state cybol name. */
static wchar_t* MASK_EVENT_GUI_STATE_CYBOL_NAME = L"mask";
static int* MASK_EVENT_GUI_STATE_CYBOL_NAME_COUNT = NUMBER_4_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The mode event gui state cybol name. */
static wchar_t* MODE_EVENT_GUI_STATE_CYBOL_NAME = L"mode";
static int* MODE_EVENT_GUI_STATE_CYBOL_NAME_COUNT = NUMBER_4_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The window event gui state cybol name. */
static wchar_t* WINDOW_EVENT_GUI_STATE_CYBOL_NAME = L"window";
static int* WINDOW_EVENT_GUI_STATE_CYBOL_NAME_COUNT = NUMBER_6_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The x event gui state cybol name. */
static wchar_t* X_EVENT_GUI_STATE_CYBOL_NAME = L"x";
static int* X_EVENT_GUI_STATE_CYBOL_NAME_COUNT = NUMBER_1_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The y event gui state cybol name. */
static wchar_t* Y_EVENT_GUI_STATE_CYBOL_NAME = L"y";
static int* Y_EVENT_GUI_STATE_CYBOL_NAME_COUNT = NUMBER_1_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/* EVENT_GUI_STATE_CYBOL_NAME_CONSTANT_HEADER */
#endif
