/*
 * Copyright (C) 1999-2023. Christian Heller.
 *
 * This file is part of the Cybernetics Oriented Interpreter (CYBOI).
 *
 * CYBOI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * CYBOI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CYBOI. If not, see <http://www.gnu.org/licenses/>.
 *
 * Cybernetics Oriented Programming (CYBOP) <http://www.cybop.org/>
 * CYBOP Developers <cybop-developers@nongnu.org>
 *
 * @version CYBOP 0.27.0 2023-08-31
 * @author Christian Heller <christian.heller@cybop.org>
 */

#ifndef KEYBOARD_STATE_CYBOL_NAME_CONSTANT_HEADER
#define KEYBOARD_STATE_CYBOL_NAME_CONSTANT_HEADER

//
// System interface
//

#include <stddef.h> // wchar_t

//
// Library interface
//

#include "constant.h"

/** The arrow-down keyboard state cybol name. */
static wchar_t* ARROW_DOWN_KEYBOARD_STATE_CYBOL_NAME = L"arrow-down";
static int* ARROW_DOWN_KEYBOARD_STATE_CYBOL_NAME_COUNT = NUMBER_10_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The arrow-left keyboard state cybol name. */
static wchar_t* ARROW_LEFT_KEYBOARD_STATE_CYBOL_NAME = L"arrow-left";
static int* ARROW_LEFT_KEYBOARD_STATE_CYBOL_NAME_COUNT = NUMBER_10_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The arrow-right keyboard state cybol name. */
static wchar_t* ARROW_RIGHT_KEYBOARD_STATE_CYBOL_NAME = L"arrow-right";
static int* ARROW_RIGHT_KEYBOARD_STATE_CYBOL_NAME_COUNT = NUMBER_11_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The arrow-up keyboard state cybol name. */
static wchar_t* ARROW_UP_KEYBOARD_STATE_CYBOL_NAME = L"arrow-up";
static int* ARROW_UP_KEYBOARD_STATE_CYBOL_NAME_COUNT = NUMBER_8_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The enter keyboard state cybol name. */
static wchar_t* ENTER_KEYBOARD_STATE_CYBOL_NAME = L"enter";
static int* ENTER_KEYBOARD_STATE_CYBOL_NAME_COUNT = NUMBER_5_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The escape keyboard state cybol name. */
static wchar_t* ESCAPE_KEYBOARD_STATE_CYBOL_NAME = L"escape";
static int* ESCAPE_KEYBOARD_STATE_CYBOL_NAME_COUNT = NUMBER_6_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/* KEYBOARD_STATE_CYBOL_NAME_CONSTANT_HEADER */
#endif
