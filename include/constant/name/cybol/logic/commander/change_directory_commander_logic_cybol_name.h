/*
 * Copyright (C) 1999-2023. Christian Heller.
 *
 * This file is part of the Cybernetics Oriented Interpreter (CYBOI).
 *
 * CYBOI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * CYBOI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CYBOI. If not, see <http://www.gnu.org/licenses/>.
 *
 * Cybernetics Oriented Programming (CYBOP) <http://www.cybop.org/>
 * CYBOP Developers <cybop-developers@nongnu.org>
 *
 * @version CYBOP 0.27.0 2023-08-31
 * @author Christian Heller <christian.heller@cybop.org>
 */

#ifndef CHANGE_DIRECTORY_COMMANDER_LOGIC_CYBOL_NAME_CONSTANT_HEADER
#define CHANGE_DIRECTORY_COMMANDER_LOGIC_CYBOL_NAME_CONSTANT_HEADER

//
// System interface
//

#include <stddef.h> // wchar_t

//
// Library interface
//

#include "constant.h"

/** The directory parameter for the change directory logic in cybol. */
static wchar_t* DIRECTORY_CHANGE_DIRECTORY_COMMANDER_LOGIC_CYBOL_NAME = L"directory";
static int* DIRECTORY_CHANGE_DIRECTORY_COMMANDER_LOGIC_CYBOL_NAME_COUNT = NUMBER_9_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The not follow symbolic link option for the change directory logic in cybol. */
static wchar_t* NOT_FOLLOW_LINK_CHANGE_DIRECTORY_COMMANDER_LOGIC_CYBOL_NAME = L"not-follow-link";
static int* NOT_FOLLOW_LINK_CHANGE_DIRECTORY_COMMANDER_LOGIC_CYBOL_NAME_COUNT = NUMBER_15_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The follow symbolic link option for the change directory logic in cybol. */
static wchar_t* FOLLOW_LINK_CHANGE_DIRECTORY_COMMANDER_LOGIC_CYBOL_NAME = L"follow-link";
static int* FOLLOW_LINK_CHANGE_DIRECTORY_COMMANDER_LOGIC_CYBOL_NAME_COUNT = NUMBER_11_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The change drive option for the change directory logic in cybol. */
static wchar_t* CHANGE_DRIVE_CHANGE_DIRECTORY_COMMANDER_LOGIC_CYBOL_NAME = L"change-drive";
static int* CHANGE_DRIVE_CHANGE_DIRECTORY_COMMANDER_LOGIC_CYBOL_NAME_COUNT = NUMBER_12_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/* CHANGE_DIRECTORY_COMMANDER_LOGIC_CYBOL_NAME_CONSTANT_HEADER */
#endif
