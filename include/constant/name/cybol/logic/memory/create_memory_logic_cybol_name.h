/*
 * Copyright (C) 1999-2023. Christian Heller.
 *
 * This file is part of the Cybernetics Oriented Interpreter (CYBOI).
 *
 * CYBOI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * CYBOI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CYBOI. If not, see <http://www.gnu.org/licenses/>.
 *
 * Cybernetics Oriented Programming (CYBOP) <http://www.cybop.org/>
 * CYBOP Developers <cybop-developers@nongnu.org>
 *
 * @version CYBOP 0.27.0 2023-08-31
 * @author Christian Heller <christian.heller@cybop.org>
 */

#ifndef CREATE_MEMORY_LOGIC_CYBOL_NAME_CONSTANT_HEADER
#define CREATE_MEMORY_LOGIC_CYBOL_NAME_CONSTANT_HEADER

//
// System interface
//

#include <stddef.h> // wchar_t

//
// Library interface
//

#include "constant.h"

/** The name create memory logic cybol name. */
static wchar_t* NAME_CREATE_MEMORY_LOGIC_CYBOL_NAME = L"name";
static int* NAME_CREATE_MEMORY_LOGIC_CYBOL_NAME_COUNT = NUMBER_4_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The format create memory logic cybol name. */
static wchar_t* FORMAT_CREATE_MEMORY_LOGIC_CYBOL_NAME = L"format";
static int* FORMAT_CREATE_MEMORY_LOGIC_CYBOL_NAME_COUNT = NUMBER_6_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The whole create memory logic cybol name. */
static wchar_t* WHOLE_CREATE_MEMORY_LOGIC_CYBOL_NAME = L"whole";
static int* WHOLE_CREATE_MEMORY_LOGIC_CYBOL_NAME_COUNT = NUMBER_5_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The properties create memory logic cybol name. */
static wchar_t* PROPERTIES_CREATE_MEMORY_LOGIC_CYBOL_NAME = L"properties";
static int* PROPERTIES_CREATE_MEMORY_LOGIC_CYBOL_NAME_COUNT = NUMBER_10_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/* CREATE_MEMORY_LOGIC_CYBOL_NAME_CONSTANT_HEADER */
#endif
