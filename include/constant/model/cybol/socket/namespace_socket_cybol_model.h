/*
 * Copyright (C) 1999-2023. Christian Heller.
 *
 * This file is part of the Cybernetics Oriented Interpreter (CYBOI).
 *
 * CYBOI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * CYBOI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CYBOI. If not, see <http://www.gnu.org/licenses/>.
 *
 * Cybernetics Oriented Programming (CYBOP) <http://www.cybop.org/>
 * CYBOP Developers <cybop-developers@nongnu.org>
 *
 * @version CYBOP 0.27.0 2023-08-31
 * @author Christian Heller <christian.heller@cybop.org>
 */

#ifndef NAMESPACE_SOCKET_CYBOL_MODEL_CONSTANT_HEADER
#define NAMESPACE_SOCKET_CYBOL_MODEL_CONSTANT_HEADER

//
// System interface
//

#include <stddef.h> // wchar_t

//
// Library interface
//

#include "constant.h"

/** The appletalk namespace socket cybol model. */
static wchar_t* APPLETALK_NAMESPACE_SOCKET_CYBOL_MODEL = L"appletalk";
static int* APPLETALK_NAMESPACE_SOCKET_CYBOL_MODEL_COUNT = NUMBER_9_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The bluetooth namespace socket cybol model. */
static wchar_t* BLUETOOTH_NAMESPACE_SOCKET_CYBOL_MODEL = L"bluetooth";
static int* BLUETOOTH_NAMESPACE_SOCKET_CYBOL_MODEL_COUNT = NUMBER_9_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The ccitt namespace socket cybol model. */
static wchar_t* CCITT_NAMESPACE_SOCKET_CYBOL_MODEL = L"ccitt";
static int* CCITT_NAMESPACE_SOCKET_CYBOL_MODEL_COUNT = NUMBER_5_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The internet model processor (implink) namespace socket cybol model. */
static wchar_t* IMPLINK_NAMESPACE_SOCKET_CYBOL_MODEL = L"implink";
static int* IMPLINK_NAMESPACE_SOCKET_CYBOL_MODEL_COUNT = NUMBER_7_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The internet protocol version 4 (ipv4) namespace socket cybol model. */
static wchar_t* INET_NAMESPACE_SOCKET_CYBOL_MODEL = L"ipv4";
static int* INET_NAMESPACE_SOCKET_CYBOL_MODEL_COUNT = NUMBER_4_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The internet protocol version 6 (ipv6) namespace socket cybol model. */
static wchar_t* INET6_NAMESPACE_SOCKET_CYBOL_MODEL = L"ipv6";
static int* INET6_NAMESPACE_SOCKET_CYBOL_MODEL_COUNT = NUMBER_4_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The internetwork packet exchange (ipx) namespace socket cybol model. */
static wchar_t* IPX_NAMESPACE_SOCKET_CYBOL_MODEL = L"ipx";
static int* IPX_NAMESPACE_SOCKET_CYBOL_MODEL_COUNT = NUMBER_3_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The infrared data association (irda) namespace socket cybol model. */
static wchar_t* IRDA_NAMESPACE_SOCKET_CYBOL_MODEL = L"irda";
static int* IRDA_NAMESPACE_SOCKET_CYBOL_MODEL_COUNT = NUMBER_4_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The international organization for standardization (iso) open systems interconnect (osi) namespace socket cybol model. */
static wchar_t* ISO_NAMESPACE_SOCKET_CYBOL_MODEL = L"iso";
static int* ISO_NAMESPACE_SOCKET_CYBOL_MODEL_COUNT = NUMBER_3_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The local namespace socket cybol model. */
static wchar_t* LOCAL_NAMESPACE_SOCKET_CYBOL_MODEL = L"local";
static int* LOCAL_NAMESPACE_SOCKET_CYBOL_MODEL_COUNT = NUMBER_5_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The netbios namespace socket cybol model. */
static wchar_t* NETBIOS_NAMESPACE_SOCKET_CYBOL_MODEL = L"netbios";
static int* NETBIOS_NAMESPACE_SOCKET_CYBOL_MODEL_COUNT = NUMBER_7_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The xerox network software protocols namespace socket cybol model. */
static wchar_t* NS_NAMESPACE_SOCKET_CYBOL_MODEL = L"ns";
static int* NS_NAMESPACE_SOCKET_CYBOL_MODEL_COUNT = NUMBER_2_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The local area routing protocol (route) namespace socket cybol model. */
static wchar_t* ROUTE_NAMESPACE_SOCKET_CYBOL_MODEL = L"route";
static int* ROUTE_NAMESPACE_SOCKET_CYBOL_MODEL_COUNT = NUMBER_5_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/* NAMESPACE_SOCKET_CYBOL_MODEL_CONSTANT_HEADER */
#endif
