/*
 * Copyright (C) 1999-2023. Christian Heller.
 *
 * This file is part of the Cybernetics Oriented Interpreter (CYBOI).
 *
 * CYBOI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * CYBOI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CYBOI. If not, see <http://www.gnu.org/licenses/>.
 *
 * Cybernetics Oriented Programming (CYBOP) <http://www.cybop.org/>
 * CYBOP Developers <cybop-developers@nongnu.org>
 *
 * @version CYBOP 0.27.0 2023-08-31
 * @author Christian Heller <christian.heller@cybop.org>
 */

#ifndef DOUBLE_STATE_CYBOI_MODEL_CONSTANT_HEADER
#define DOUBLE_STATE_CYBOI_MODEL_CONSTANT_HEADER

//
// Numbers 0.0 - 0.9.
//

/** The number 0.0 double state cyboi model. */
static double NUMBER_0_0_DOUBLE_STATE_CYBOI_MODEL_ARRAY[] = { 0.0 };
static double* NUMBER_0_0_DOUBLE_STATE_CYBOI_MODEL = NUMBER_0_0_DOUBLE_STATE_CYBOI_MODEL_ARRAY;

/** The number 0.1 double state cyboi model. */
static double NUMBER_0_1_DOUBLE_STATE_CYBOI_MODEL_ARRAY[] = { 0.1 };
static double* NUMBER_0_1_DOUBLE_STATE_CYBOI_MODEL = NUMBER_0_1_DOUBLE_STATE_CYBOI_MODEL_ARRAY;

/** The number 0.2 double state cyboi model. */
static double NUMBER_0_2_DOUBLE_STATE_CYBOI_MODEL_ARRAY[] = { 0.2 };
static double* NUMBER_0_2_DOUBLE_STATE_CYBOI_MODEL = NUMBER_0_2_DOUBLE_STATE_CYBOI_MODEL_ARRAY;

/** The number 0.3 double state cyboi model. */
static double NUMBER_0_3_DOUBLE_STATE_CYBOI_MODEL_ARRAY[] = { 0.3 };
static double* NUMBER_0_3_DOUBLE_STATE_CYBOI_MODEL = NUMBER_0_3_DOUBLE_STATE_CYBOI_MODEL_ARRAY;

/** The number 0.4 double state cyboi model. */
static double NUMBER_0_4_DOUBLE_STATE_CYBOI_MODEL_ARRAY[] = { 0.4 };
static double* NUMBER_0_4_DOUBLE_STATE_CYBOI_MODEL = NUMBER_0_4_DOUBLE_STATE_CYBOI_MODEL_ARRAY;

/** The number 0.5 double state cyboi model. */
static double NUMBER_0_5_DOUBLE_STATE_CYBOI_MODEL_ARRAY[] = { 0.5 };
static double* NUMBER_0_5_DOUBLE_STATE_CYBOI_MODEL = NUMBER_0_5_DOUBLE_STATE_CYBOI_MODEL_ARRAY;

/** The number 0.6 double state cyboi model. */
static double NUMBER_0_6_DOUBLE_STATE_CYBOI_MODEL_ARRAY[] = { 0.6 };
static double* NUMBER_0_6_DOUBLE_STATE_CYBOI_MODEL = NUMBER_0_6_DOUBLE_STATE_CYBOI_MODEL_ARRAY;

/** The number 0.7 double state cyboi model. */
static double NUMBER_0_7_DOUBLE_STATE_CYBOI_MODEL_ARRAY[] = { 0.7 };
static double* NUMBER_0_7_DOUBLE_STATE_CYBOI_MODEL = NUMBER_0_7_DOUBLE_STATE_CYBOI_MODEL_ARRAY;

/** The number 0.8 double state cyboi model. */
static double NUMBER_0_8_DOUBLE_STATE_CYBOI_MODEL_ARRAY[] = { 0.8 };
static double* NUMBER_0_8_DOUBLE_STATE_CYBOI_MODEL = NUMBER_0_8_DOUBLE_STATE_CYBOI_MODEL_ARRAY;

/** The number 0.9 double state cyboi model. */
static double NUMBER_0_9_DOUBLE_STATE_CYBOI_MODEL_ARRAY[] = { 0.9 };
static double* NUMBER_0_9_DOUBLE_STATE_CYBOI_MODEL = NUMBER_0_9_DOUBLE_STATE_CYBOI_MODEL_ARRAY;

//
// Numbers 1.0 - 1.9.
//

/** The number 1.0 double state cyboi model. */
static double NUMBER_1_0_DOUBLE_STATE_CYBOI_MODEL_ARRAY[] = { 1.0 };
static double* NUMBER_1_0_DOUBLE_STATE_CYBOI_MODEL = NUMBER_1_0_DOUBLE_STATE_CYBOI_MODEL_ARRAY;

//
// Numbers 2.0 - 2.9.
//

/** The number 2.0 double state cyboi model. */
static double NUMBER_2_0_DOUBLE_STATE_CYBOI_MODEL_ARRAY[] = { 2.0 };
static double* NUMBER_2_0_DOUBLE_STATE_CYBOI_MODEL = NUMBER_2_0_DOUBLE_STATE_CYBOI_MODEL_ARRAY;

//
// Numbers 10.0 - 10.9.
//

/** The number 10.0 double state cyboi model. */
static double NUMBER_10_0_DOUBLE_STATE_CYBOI_MODEL_ARRAY[] = { 10.0 };
static double* NUMBER_10_0_DOUBLE_STATE_CYBOI_MODEL = NUMBER_10_0_DOUBLE_STATE_CYBOI_MODEL_ARRAY;

/* DOUBLE_STATE_CYBOI_MODEL_CONSTANT_HEADER */
#endif
