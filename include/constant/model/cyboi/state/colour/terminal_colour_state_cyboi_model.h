/*
 * Copyright (C) 1999-2023. Christian Heller.
 *
 * This file is part of the Cybernetics Oriented Interpreter (CYBOI).
 *
 * CYBOI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * CYBOI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CYBOI. If not, see <http://www.gnu.org/licenses/>.
 *
 * Cybernetics Oriented Programming (CYBOP) <http://www.cybop.org/>
 * CYBOP Developers <cybop-developers@nongnu.org>
 *
 * @version CYBOP 0.27.0 2023-08-31
 * @author Christian Heller <christian.heller@cybop.org>
 */

#ifndef TERMINAL_COLOUR_STATE_CYBOI_MODEL_CONSTANT_HEADER
#define TERMINAL_COLOUR_STATE_CYBOI_MODEL_CONSTANT_HEADER

//
// Library interface
//

#include "constant.h"

/** The black terminal colour state cyboi model. */
static int* BLACK_TERMINAL_COLOUR_STATE_CYBOI_MODEL = NUMBER_0_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The blue terminal colour state cyboi model. */
static int* BLUE_TERMINAL_COLOUR_STATE_CYBOI_MODEL = NUMBER_1_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The cyan terminal colour state cyboi model. */
static int* CYAN_TERMINAL_COLOUR_STATE_CYBOI_MODEL = NUMBER_2_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The green terminal colour state cyboi model. */
static int* GREEN_TERMINAL_COLOUR_STATE_CYBOI_MODEL = NUMBER_3_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The magenta terminal colour state cyboi model. */
static int* MAGENTA_TERMINAL_COLOUR_STATE_CYBOI_MODEL = NUMBER_4_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The red terminal colour state cyboi model. */
static int* RED_TERMINAL_COLOUR_STATE_CYBOI_MODEL = NUMBER_5_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The white terminal colour state cyboi model. */
static int* WHITE_TERMINAL_COLOUR_STATE_CYBOI_MODEL = NUMBER_6_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The yellow terminal colour state cyboi model. */
static int* YELLOW_TERMINAL_COLOUR_STATE_CYBOI_MODEL = NUMBER_7_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/* TERMINAL_COLOUR_STATE_CYBOI_MODEL_CONSTANT_HEADER */
#endif
