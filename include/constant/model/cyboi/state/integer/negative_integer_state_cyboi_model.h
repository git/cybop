/*
 * Copyright (C) 1999-2023. Christian Heller.
 *
 * This file is part of the Cybernetics Oriented Interpreter (CYBOI).
 *
 * CYBOI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * CYBOI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CYBOI. If not, see <http://www.gnu.org/licenses/>.
 *
 * Cybernetics Oriented Programming (CYBOP) <http://www.cybop.org/>
 * CYBOP Developers <cybop-developers@nongnu.org>
 *
 * @version CYBOP 0.27.0 2023-08-31
 * @author Christian Heller <christian.heller@cybop.org>
 */

#ifndef NEGATIVE_INTEGER_STATE_CYBOI_MODEL_CONSTANT_HEADER
#define NEGATIVE_INTEGER_STATE_CYBOI_MODEL_CONSTANT_HEADER

/** The number -1 integer state cyboi model. */
static int NUMBER_MINUS_1_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { -1 };
static int* NUMBER_MINUS_1_INTEGER_STATE_CYBOI_MODEL = NUMBER_MINUS_1_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number -2 integer state cyboi model. */
static int NUMBER_MINUS_2_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { -2 };
static int* NUMBER_MINUS_2_INTEGER_STATE_CYBOI_MODEL = NUMBER_MINUS_2_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/* NEGATIVE_INTEGER_STATE_CYBOI_MODEL_CONSTANT_HEADER */
#endif
