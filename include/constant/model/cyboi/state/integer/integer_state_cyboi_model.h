/*
 * Copyright (C) 1999-2023. Christian Heller.
 *
 * This file is part of the Cybernetics Oriented Interpreter (CYBOI).
 *
 * CYBOI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * CYBOI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CYBOI. If not, see <http://www.gnu.org/licenses/>.
 *
 * Cybernetics Oriented Programming (CYBOP) <http://www.cybop.org/>
 * CYBOP Developers <cybop-developers@nongnu.org>
 *
 * @version CYBOP 0.27.0 2023-08-31
 * @author Christian Heller <christian.heller@cybop.org>
 */

#ifndef INTEGER_STATE_CYBOI_MODEL_CONSTANT_HEADER
#define INTEGER_STATE_CYBOI_MODEL_CONSTANT_HEADER

/** The number 0 integer state cyboi model. */
static int NUMBER_0_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 0 };
static int* NUMBER_0_INTEGER_STATE_CYBOI_MODEL = NUMBER_0_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 1 integer state cyboi model. */
static int NUMBER_1_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 1 };
static int* NUMBER_1_INTEGER_STATE_CYBOI_MODEL = NUMBER_1_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 2 integer state cyboi model. */
static int NUMBER_2_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 2 };
static int* NUMBER_2_INTEGER_STATE_CYBOI_MODEL = NUMBER_2_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 3 integer state cyboi model. */
static int NUMBER_3_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 3 };
static int* NUMBER_3_INTEGER_STATE_CYBOI_MODEL = NUMBER_3_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 4 integer state cyboi model. */
static int NUMBER_4_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 4 };
static int* NUMBER_4_INTEGER_STATE_CYBOI_MODEL = NUMBER_4_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 5 integer state cyboi model. */
static int NUMBER_5_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 5 };
static int* NUMBER_5_INTEGER_STATE_CYBOI_MODEL = NUMBER_5_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 6 integer state cyboi model. */
static int NUMBER_6_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 6 };
static int* NUMBER_6_INTEGER_STATE_CYBOI_MODEL = NUMBER_6_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 7 integer state cyboi model. */
static int NUMBER_7_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 7 };
static int* NUMBER_7_INTEGER_STATE_CYBOI_MODEL = NUMBER_7_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 8 integer state cyboi model. */
static int NUMBER_8_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 8 };
static int* NUMBER_8_INTEGER_STATE_CYBOI_MODEL = NUMBER_8_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 9 integer state cyboi model. */
static int NUMBER_9_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 9 };
static int* NUMBER_9_INTEGER_STATE_CYBOI_MODEL = NUMBER_9_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 10 integer state cyboi model. */
static int NUMBER_10_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 10 };
static int* NUMBER_10_INTEGER_STATE_CYBOI_MODEL = NUMBER_10_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 11 integer state cyboi model. */
static int NUMBER_11_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 11 };
static int* NUMBER_11_INTEGER_STATE_CYBOI_MODEL = NUMBER_11_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 12 integer state cyboi model. */
static int NUMBER_12_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 12 };
static int* NUMBER_12_INTEGER_STATE_CYBOI_MODEL = NUMBER_12_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 13 integer state cyboi model. */
static int NUMBER_13_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 13 };
static int* NUMBER_13_INTEGER_STATE_CYBOI_MODEL = NUMBER_13_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 14 integer state cyboi model. */
static int NUMBER_14_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 14 };
static int* NUMBER_14_INTEGER_STATE_CYBOI_MODEL = NUMBER_14_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 15 integer state cyboi model. */
static int NUMBER_15_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 15 };
static int* NUMBER_15_INTEGER_STATE_CYBOI_MODEL = NUMBER_15_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 16 integer state cyboi model. */
static int NUMBER_16_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 16 };
static int* NUMBER_16_INTEGER_STATE_CYBOI_MODEL = NUMBER_16_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 17 integer state cyboi model. */
static int NUMBER_17_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 17 };
static int* NUMBER_17_INTEGER_STATE_CYBOI_MODEL = NUMBER_17_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 18 integer state cyboi model. */
static int NUMBER_18_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 18 };
static int* NUMBER_18_INTEGER_STATE_CYBOI_MODEL = NUMBER_18_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 19 integer state cyboi model. */
static int NUMBER_19_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 19 };
static int* NUMBER_19_INTEGER_STATE_CYBOI_MODEL = NUMBER_19_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 20 integer state cyboi model. */
static int NUMBER_20_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 20 };
static int* NUMBER_20_INTEGER_STATE_CYBOI_MODEL = NUMBER_20_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 21 integer state cyboi model. */
static int NUMBER_21_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 21 };
static int* NUMBER_21_INTEGER_STATE_CYBOI_MODEL = NUMBER_21_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 22 integer state cyboi model. */
static int NUMBER_22_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 22 };
static int* NUMBER_22_INTEGER_STATE_CYBOI_MODEL = NUMBER_22_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 23 integer state cyboi model. */
static int NUMBER_23_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 23 };
static int* NUMBER_23_INTEGER_STATE_CYBOI_MODEL = NUMBER_23_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 24 integer state cyboi model. */
static int NUMBER_24_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 24 };
static int* NUMBER_24_INTEGER_STATE_CYBOI_MODEL = NUMBER_24_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 25 integer state cyboi model. */
static int NUMBER_25_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 25 };
static int* NUMBER_25_INTEGER_STATE_CYBOI_MODEL = NUMBER_25_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 26 integer state cyboi model. */
static int NUMBER_26_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 26 };
static int* NUMBER_26_INTEGER_STATE_CYBOI_MODEL = NUMBER_26_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 27 integer state cyboi model. */
static int NUMBER_27_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 27 };
static int* NUMBER_27_INTEGER_STATE_CYBOI_MODEL = NUMBER_27_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 28 integer state cyboi model. */
static int NUMBER_28_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 28 };
static int* NUMBER_28_INTEGER_STATE_CYBOI_MODEL = NUMBER_28_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 29 integer state cyboi model. */
static int NUMBER_29_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 29 };
static int* NUMBER_29_INTEGER_STATE_CYBOI_MODEL = NUMBER_29_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 30 integer state cyboi model. */
static int NUMBER_30_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 30 };
static int* NUMBER_30_INTEGER_STATE_CYBOI_MODEL = NUMBER_30_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 31 integer state cyboi model. */
static int NUMBER_31_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 31 };
static int* NUMBER_31_INTEGER_STATE_CYBOI_MODEL = NUMBER_31_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 32 integer state cyboi model. */
static int NUMBER_32_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 32 };
static int* NUMBER_32_INTEGER_STATE_CYBOI_MODEL = NUMBER_32_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 33 integer state cyboi model. */
static int NUMBER_33_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 33 };
static int* NUMBER_33_INTEGER_STATE_CYBOI_MODEL = NUMBER_33_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 34 integer state cyboi model. */
static int NUMBER_34_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 34 };
static int* NUMBER_34_INTEGER_STATE_CYBOI_MODEL = NUMBER_34_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 35 integer state cyboi model. */
static int NUMBER_35_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 35 };
static int* NUMBER_35_INTEGER_STATE_CYBOI_MODEL = NUMBER_35_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 36 integer state cyboi model. */
static int NUMBER_36_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 36 };
static int* NUMBER_36_INTEGER_STATE_CYBOI_MODEL = NUMBER_36_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 37 integer state cyboi model. */
static int NUMBER_37_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 37 };
static int* NUMBER_37_INTEGER_STATE_CYBOI_MODEL = NUMBER_37_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 38 integer state cyboi model. */
static int NUMBER_38_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 38 };
static int* NUMBER_38_INTEGER_STATE_CYBOI_MODEL = NUMBER_38_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 39 integer state cyboi model. */
static int NUMBER_39_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 39 };
static int* NUMBER_39_INTEGER_STATE_CYBOI_MODEL = NUMBER_39_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 40 integer state cyboi model. */
static int NUMBER_40_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 40 };
static int* NUMBER_40_INTEGER_STATE_CYBOI_MODEL = NUMBER_40_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 41 integer state cyboi model. */
static int NUMBER_41_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 41 };
static int* NUMBER_41_INTEGER_STATE_CYBOI_MODEL = NUMBER_41_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 42 integer state cyboi model. */
static int NUMBER_42_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 42 };
static int* NUMBER_42_INTEGER_STATE_CYBOI_MODEL = NUMBER_42_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 43 integer state cyboi model. */
static int NUMBER_43_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 43 };
static int* NUMBER_43_INTEGER_STATE_CYBOI_MODEL = NUMBER_43_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 44 integer state cyboi model. */
static int NUMBER_44_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 44 };
static int* NUMBER_44_INTEGER_STATE_CYBOI_MODEL = NUMBER_44_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 45 integer state cyboi model. */
static int NUMBER_45_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 45 };
static int* NUMBER_45_INTEGER_STATE_CYBOI_MODEL = NUMBER_45_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 46 integer state cyboi model. */
static int NUMBER_46_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 46 };
static int* NUMBER_46_INTEGER_STATE_CYBOI_MODEL = NUMBER_46_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 47 integer state cyboi model. */
static int NUMBER_47_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 47 };
static int* NUMBER_47_INTEGER_STATE_CYBOI_MODEL = NUMBER_47_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 48 integer state cyboi model. */
static int NUMBER_48_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 48 };
static int* NUMBER_48_INTEGER_STATE_CYBOI_MODEL = NUMBER_48_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 49 integer state cyboi model. */
static int NUMBER_49_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 49 };
static int* NUMBER_49_INTEGER_STATE_CYBOI_MODEL = NUMBER_49_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 50 integer state cyboi model. */
static int NUMBER_50_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 50 };
static int* NUMBER_50_INTEGER_STATE_CYBOI_MODEL = NUMBER_50_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 51 integer state cyboi model. */
static int NUMBER_51_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 51 };
static int* NUMBER_51_INTEGER_STATE_CYBOI_MODEL = NUMBER_51_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 52 integer state cyboi model. */
static int NUMBER_52_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 52 };
static int* NUMBER_52_INTEGER_STATE_CYBOI_MODEL = NUMBER_52_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 53 integer state cyboi model. */
static int NUMBER_53_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 53 };
static int* NUMBER_53_INTEGER_STATE_CYBOI_MODEL = NUMBER_53_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 54 integer state cyboi model. */
static int NUMBER_54_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 54 };
static int* NUMBER_54_INTEGER_STATE_CYBOI_MODEL = NUMBER_54_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 55 integer state cyboi model. */
static int NUMBER_55_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 55 };
static int* NUMBER_55_INTEGER_STATE_CYBOI_MODEL = NUMBER_55_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 56 integer state cyboi model. */
static int NUMBER_56_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 56 };
static int* NUMBER_56_INTEGER_STATE_CYBOI_MODEL = NUMBER_56_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 57 integer state cyboi model. */
static int NUMBER_57_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 57 };
static int* NUMBER_57_INTEGER_STATE_CYBOI_MODEL = NUMBER_57_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 58 integer state cyboi model. */
static int NUMBER_58_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 58 };
static int* NUMBER_58_INTEGER_STATE_CYBOI_MODEL = NUMBER_58_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 59 integer state cyboi model. */
static int NUMBER_59_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 59 };
static int* NUMBER_59_INTEGER_STATE_CYBOI_MODEL = NUMBER_59_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 60 integer state cyboi model. */
static int NUMBER_60_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 60 };
static int* NUMBER_60_INTEGER_STATE_CYBOI_MODEL = NUMBER_60_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 61 integer state cyboi model. */
static int NUMBER_61_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 61 };
static int* NUMBER_61_INTEGER_STATE_CYBOI_MODEL = NUMBER_61_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 62 integer state cyboi model. */
static int NUMBER_62_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 62 };
static int* NUMBER_62_INTEGER_STATE_CYBOI_MODEL = NUMBER_62_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 63 integer state cyboi model. */
static int NUMBER_63_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 63 };
static int* NUMBER_63_INTEGER_STATE_CYBOI_MODEL = NUMBER_63_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 64 integer state cyboi model. */
static int NUMBER_64_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 64 };
static int* NUMBER_64_INTEGER_STATE_CYBOI_MODEL = NUMBER_64_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 65 integer state cyboi model. */
static int NUMBER_65_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 65 };
static int* NUMBER_65_INTEGER_STATE_CYBOI_MODEL = NUMBER_65_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 66 integer state cyboi model. */
static int NUMBER_66_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 66 };
static int* NUMBER_66_INTEGER_STATE_CYBOI_MODEL = NUMBER_66_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 67 integer state cyboi model. */
static int NUMBER_67_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 67 };
static int* NUMBER_67_INTEGER_STATE_CYBOI_MODEL = NUMBER_67_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 68 integer state cyboi model. */
static int NUMBER_68_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 68 };
static int* NUMBER_68_INTEGER_STATE_CYBOI_MODEL = NUMBER_68_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 69 integer state cyboi model. */
static int NUMBER_69_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 69 };
static int* NUMBER_69_INTEGER_STATE_CYBOI_MODEL = NUMBER_69_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 70 integer state cyboi model. */
static int NUMBER_70_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 70 };
static int* NUMBER_70_INTEGER_STATE_CYBOI_MODEL = NUMBER_70_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 71 integer state cyboi model. */
static int NUMBER_71_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 71 };
static int* NUMBER_71_INTEGER_STATE_CYBOI_MODEL = NUMBER_71_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 72 integer state cyboi model. */
static int NUMBER_72_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 72 };
static int* NUMBER_72_INTEGER_STATE_CYBOI_MODEL = NUMBER_72_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 73 integer state cyboi model. */
static int NUMBER_73_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 73 };
static int* NUMBER_73_INTEGER_STATE_CYBOI_MODEL = NUMBER_73_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 74 integer state cyboi model. */
static int NUMBER_74_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 74 };
static int* NUMBER_74_INTEGER_STATE_CYBOI_MODEL = NUMBER_74_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 75 integer state cyboi model. */
static int NUMBER_75_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 75 };
static int* NUMBER_75_INTEGER_STATE_CYBOI_MODEL = NUMBER_75_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 76 integer state cyboi model. */
static int NUMBER_76_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 76 };
static int* NUMBER_76_INTEGER_STATE_CYBOI_MODEL = NUMBER_76_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 77 integer state cyboi model. */
static int NUMBER_77_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 77 };
static int* NUMBER_77_INTEGER_STATE_CYBOI_MODEL = NUMBER_77_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 78 integer state cyboi model. */
static int NUMBER_78_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 78 };
static int* NUMBER_78_INTEGER_STATE_CYBOI_MODEL = NUMBER_78_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 79 integer state cyboi model. */
static int NUMBER_79_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 79 };
static int* NUMBER_79_INTEGER_STATE_CYBOI_MODEL = NUMBER_79_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 80 integer state cyboi model. */
static int NUMBER_80_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 80 };
static int* NUMBER_80_INTEGER_STATE_CYBOI_MODEL = NUMBER_80_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 81 integer state cyboi model. */
static int NUMBER_81_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 81 };
static int* NUMBER_81_INTEGER_STATE_CYBOI_MODEL = NUMBER_81_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 82 integer state cyboi model. */
static int NUMBER_82_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 82 };
static int* NUMBER_82_INTEGER_STATE_CYBOI_MODEL = NUMBER_82_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 83 integer state cyboi model. */
static int NUMBER_83_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 83 };
static int* NUMBER_83_INTEGER_STATE_CYBOI_MODEL = NUMBER_83_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 84 integer state cyboi model. */
static int NUMBER_84_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 84 };
static int* NUMBER_84_INTEGER_STATE_CYBOI_MODEL = NUMBER_84_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 85 integer state cyboi model. */
static int NUMBER_85_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 85 };
static int* NUMBER_85_INTEGER_STATE_CYBOI_MODEL = NUMBER_85_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 86 integer state cyboi model. */
static int NUMBER_86_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 86 };
static int* NUMBER_86_INTEGER_STATE_CYBOI_MODEL = NUMBER_86_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 87 integer state cyboi model. */
static int NUMBER_87_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 87 };
static int* NUMBER_87_INTEGER_STATE_CYBOI_MODEL = NUMBER_87_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 88 integer state cyboi model. */
static int NUMBER_88_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 88 };
static int* NUMBER_88_INTEGER_STATE_CYBOI_MODEL = NUMBER_88_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 89 integer state cyboi model. */
static int NUMBER_89_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 89 };
static int* NUMBER_89_INTEGER_STATE_CYBOI_MODEL = NUMBER_89_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 90 integer state cyboi model. */
static int NUMBER_90_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 90 };
static int* NUMBER_90_INTEGER_STATE_CYBOI_MODEL = NUMBER_90_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 91 integer state cyboi model. */
static int NUMBER_91_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 91 };
static int* NUMBER_91_INTEGER_STATE_CYBOI_MODEL = NUMBER_91_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 92 integer state cyboi model. */
static int NUMBER_92_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 92 };
static int* NUMBER_92_INTEGER_STATE_CYBOI_MODEL = NUMBER_92_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 93 integer state cyboi model. */
static int NUMBER_93_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 93 };
static int* NUMBER_93_INTEGER_STATE_CYBOI_MODEL = NUMBER_93_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 94 integer state cyboi model. */
static int NUMBER_94_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 94 };
static int* NUMBER_94_INTEGER_STATE_CYBOI_MODEL = NUMBER_94_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 95 integer state cyboi model. */
static int NUMBER_95_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 95 };
static int* NUMBER_95_INTEGER_STATE_CYBOI_MODEL = NUMBER_95_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 96 integer state cyboi model. */
static int NUMBER_96_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 96 };
static int* NUMBER_96_INTEGER_STATE_CYBOI_MODEL = NUMBER_96_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 97 integer state cyboi model. */
static int NUMBER_97_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 97 };
static int* NUMBER_97_INTEGER_STATE_CYBOI_MODEL = NUMBER_97_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 98 integer state cyboi model. */
static int NUMBER_98_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 98 };
static int* NUMBER_98_INTEGER_STATE_CYBOI_MODEL = NUMBER_98_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/** The number 99 integer state cyboi model. */
static int NUMBER_99_INTEGER_STATE_CYBOI_MODEL_ARRAY[] = { 99 };
static int* NUMBER_99_INTEGER_STATE_CYBOI_MODEL = NUMBER_99_INTEGER_STATE_CYBOI_MODEL_ARRAY;

/* INTEGER_STATE_CYBOI_MODEL_CONSTANT_HEADER */
#endif
