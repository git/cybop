#
# Copyright (C) 1999-2023. Christian Heller.
#
# This file is the first place a user will look to get an overview for the
# purpose of cybop, and perhaps special installation instructions.
#
# Cybernetics Oriented Programming (CYBOP) <http://www.cybop.org/>
# CYBOP Developers <cybop-developers@nongnu.org>
#
# @version CYBOP 0.27.0 2023-08-31
# @author Christian Heller <christian.heller@cybop.org>
#

The CYBOP project comprises three basic components:

1 Cybernetics Oriented Programming (CYBOP): a software development theory using simple hierarchy
2 Cybernetics Oriented Language (CYBOL): a knowledge modelling and programming language based on XML and JSON
3 Cybernetics Oriented Interpreter (CYBOI): an interpreter programme written in ANSI C

Instructions on how to install cybop, compile cyboi and run cybol applications
are given in the file called INSTALL.

You may also want to have a look at our tutorials at:
http://www.cybop.org/tutorial/

Report bugs to <cybop-developers@nongnu.org> or enter them at:
http://savannah.nongnu.org/bugs/?func=additem&group=cybop

You may find further information, relevant mailing lists etc. on the website:
http://www.cybop.org/

