//
// simplified json
//
// syntax needs only brackets, no other symbols,
// i.e. no comma, curly brace, colon
//

[
    [[open_stdout] [inline] [dispatch/open] [] [
        [[channel] [inline] [meta/channel] [terminal] []]
        [[device] [inline] [text/plain] [standard-output] []]
        [[identification] [inline] [text/cybol-path] [.stdout] []]
    ]]
    [[print_greeting] [inline] [communicate/send] [] [
        [[channel] [inline] [meta/channel] [terminal] []]
        [[receiver] [inline] [text/cybol-path] [.stdout] []]
        [[language] [inline] [meta/language] [message/cli] []]
        [[format] [inline] [meta/format] [text/plain] []]
        [[message] [inline] [text/plain] [Hello World!] [
            [[foreground] [inline] [colour/terminal] [green] []]
        ]]
        [[newline] [inline] [logicvalue/boolean] true []]
    ]]
    [[exit_application] [inline] [live/exit] [] []]
]
