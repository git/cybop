<table class="table table-striped table-bordered table-hover table-sm table-responsive">
    <thead>
        <tr>
            <th>
                Titel
            </th>
            <th>
                K&#xfc;nstler
            </th>
            <th>
                Genre
            </th>
            <th>
                Jahr
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                1000 und 1 Nacht (Zoom!)
            </td>
            <td>
                Klaus Lage Band
            </td>
            <td>
                Deutschrock
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                A Walk In The Park
            </td>
            <td>
                Nick Straker Band
            </td>
            <td>
                1980er Rock-Pop 
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Africa
            </td>
            <td>
                Toto
            </td>
            <td>
                1980er Rock-Pop 
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Alles Nur In Meinem Kopf
            </td>
            <td>
                Andreas Bourani
            </td>
            <td>
                2010er Rock-Pop
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Aloha Heja He
            </td>
            <td>
                Achim Reichel
            </td>
            <td>
                Deutschrock
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Always On My Mind
            </td>
            <td>
                Elvis Presley
            </td>
            <td>
                1950er Rock &#x27;n&#x27; Roll 
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Angels
            </td>
            <td>
                Robbie Williams
            </td>
            <td>
                2000er Rock-Pop
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Anita
            </td>
            <td>
                Costa Cordalis
            </td>
            <td>
                Schlager
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Anton Aus Tirol
            </td>
            <td>
                DJ &#xd6;tzi
            </td>
            <td>
                Volksmusik 
            </td>
            <td>
                1999
            </td>
        </tr>
        <tr>
            <td>
                Apfeltraum
            </td>
            <td>
                Klaus-Renft-Combo
            </td>
            <td>
                Ostrock (DDR)
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Are You Lonesome Tonight
            </td>
            <td>
                Elvis Presley
            </td>
            <td>
                1950er Rock &#x27;n&#x27; Roll 
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Auf Der Reeperbahn Nachts Um Halb Eins
            </td>
            <td>
                Hans Albers
            </td>
            <td>
                Volksmusik 
            </td>
            <td>
                1943
            </td>
        </tr>
        <tr>
            <td>
                Bad Moon Rising
            </td>
            <td>
                C.C.R.
            </td>
            <td>
                1970er Rock 
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Capri Fischer
            </td>
            <td>
                Rudi Schuricke
            </td>
            <td>
                Schlager
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Child&#x27;s Anthem
            </td>
            <td>
                Toto
            </td>
            <td>
                1980er Rock-Pop 
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Copacabana
            </td>
            <td>
                Barry Manilow
            </td>
            <td>
                Englischer Schlager
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Country Roads
            </td>
            <td>
                John Denver
            </td>
            <td>
                Country Music
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Crying At The Discotheque
            </td>
            <td>
                Alcazar
            </td>
            <td>
                2000er Rock-Pop
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Dancing With Tears In My Eyes
            </td>
            <td>
                Ultravox
            </td>
            <td>
                1980er Rock-Pop 
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Dein Ist Mein Ganzes Herz
            </td>
            <td>
                Heinz-Rudolf Kunze
            </td>
            <td>
                Deutschrock
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Die Kleine Kneipe
            </td>
            <td>
                Peter Alexander
            </td>
            <td>
                Schlager
            </td>
            <td>
                1976
            </td>
        </tr>
        <tr>
            <td>
                Dreamin&#x27;
            </td>
            <td>
                Cliff Richard
            </td>
            <td>
                Englischer Schlager
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Du Hast Mir Gerade Noch Gefehlt
            </td>
            <td>
                Purple Schulz
            </td>
            <td>
                Deutschrock
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Ein Kompliment
            </td>
            <td>
                Sportfreunde Stiller
            </td>
            <td>
                2000er Rock-Pop
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Ein Stern (Der Deinen Namen tr&#xe4;gt)
            </td>
            <td>
                DJ &#xd6;tzi 
            </td>
            <td>
                Schlager
            </td>
            <td>
                2007
            </td>
        </tr>
        <tr>
            <td>
                Eiszeit
            </td>
            <td>
                Peter Maffay
            </td>
            <td>
                Deutschrock
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Enjoy The Silence
            </td>
            <td>
                Depeche Mode
            </td>
            <td>
                1980er Rock-Pop 
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Entre Dos Tierras
            </td>
            <td>
                Heroes del Silencio
            </td>
            <td>
                Spanischer Gitarren-Rock
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Es Gibt Kein Bier Auf Hawaii
            </td>
            <td>
                Paul Kuhn
            </td>
            <td>
                Volksmusik 
            </td>
            <td>
                1963
            </td>
        </tr>
        <tr>
            <td>
                Every Breath You Take
            </td>
            <td>
                Police
            </td>
            <td>
                1980er Rock-Pop 
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Everything&#x27;s Coming Up Roses
            </td>
            <td>
                Black
            </td>
            <td>
                1980er Rock-Pop 
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Eye In The Sky
            </td>
            <td>
                Alan Parsons Project
            </td>
            <td>
                1980er Rock-Pop 
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Feel So Close
            </td>
            <td>
                Calvin Harris
            </td>
            <td>
                2010er Rock-Pop
            </td>
            <td>
                2011
            </td>
        </tr>
        <tr>
            <td>
                Feels Like Heaven
            </td>
            <td>
                Fiction Factory
            </td>
            <td>
                1980er Rock-Pop 
            </td>
            <td>
                1985
            </td>
        </tr>
        <tr>
            <td>
                Flames Of Love
            </td>
            <td>
                Fancy
            </td>
            <td>
                1980er Rock-Pop 
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Foolish Heart
            </td>
            <td>
                Mavericks
            </td>
            <td>
                1950er Rock &#x27;n&#x27; Roll 
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Foot Of The Mountain
            </td>
            <td>
                a-ha
            </td>
            <td>
                2000er Rock-Pop
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Geronimo
            </td>
            <td>
                Sheppard
            </td>
            <td>
                2010er Rock-Pop
            </td>
            <td>
                2014
            </td>
        </tr>
        <tr>
            <td>
                Goldener Reiter
            </td>
            <td>
                Joachim Witt
            </td>
            <td>
                Neue Deutsche Welle (NDW)
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Griechischer Wein
            </td>
            <td>
                Udo J&#xfc;rgens
            </td>
            <td>
                Schlager
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Hello Again
            </td>
            <td>
                Howard Carpendale
            </td>
            <td>
                Schlager
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Hello Mary Lou
            </td>
            <td>
                Peter Kraus
            </td>
            <td>
                Schlager
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Hello Mary Lou
            </td>
            <td>
                Ricky Nelson
            </td>
            <td>
                Country Music
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Herzschlag Ist Der Takt
            </td>
            <td>
                M&#xfc;nchener Freiheit
            </td>
            <td>
                Deutschrock
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Hey Tonight
            </td>
            <td>
                C.C.R.
            </td>
            <td>
                1970er Rock 
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Hoch Auf Dem Gelben Wagen
            </td>
            <td>
                Volkslied
            </td>
            <td>
                Volksmusik 
            </td>
            <td>
                1922
            </td>
        </tr>
        <tr>
            <td>
                Hollywood Hills
            </td>
            <td>
                Sunrise Avenue
            </td>
            <td>
                2010er Rock-Pop
            </td>
            <td>
                2011
            </td>
        </tr>
        <tr>
            <td>
                I Drove All Night
            </td>
            <td>
                Roy Orbison
            </td>
            <td>
                1990er Rock-Pop 
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                I Promised Myself
            </td>
            <td>
                Nick Kamen
            </td>
            <td>
                1990er Rock-Pop 
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                I Was Made For Loving You
            </td>
            <td>
                Kiss
            </td>
            <td>
                1980er Rock-Pop 
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                I Won&#x27;t Back Down
            </td>
            <td>
                Tom Petty
            </td>
            <td>
                1990er Rock-Pop 
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                I&#x27;m On Fire
            </td>
            <td>
                Bruce Springsteen
            </td>
            <td>
                1980er Rock-Pop 
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                I&#x27;ve Been Thinking About You
            </td>
            <td>
                Londonbeat
            </td>
            <td>
                1990er Rock-Pop 
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Ich War Noch Niemals In New York
            </td>
            <td>
                Udo J&#xfc;rgens
            </td>
            <td>
                Schlager
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Jede Stunde
            </td>
            <td>
                Karat
            </td>
            <td>
                Ostrock (DDR)
            </td>
            <td>
                1982
            </td>
        </tr>
        <tr>
            <td>
                Katharine Katharine
            </td>
            <td>
                Steinwolke
            </td>
            <td>
                Neue Deutsche Welle (NDW)
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Killing Me Softly
            </td>
            <td>
                Fugees
            </td>
            <td>
                2000er Rock-Pop
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Kleine Seen
            </td>
            <td>
                Purple Schulz
            </td>
            <td>
                Deutschrock
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Kling Klang
            </td>
            <td>
                Keimzeit
            </td>
            <td>
                Ostrock (DDR)
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Knockin&#x27; On Heaven&#x27;s Door
            </td>
            <td>
                Bob Dylan
            </td>
            <td>
                1960er Beat 
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Kufsteiner Lied
            </td>
            <td>
                Volkslied
            </td>
            <td>
                Volksmusik 
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Laura Non C&#x27;e
            </td>
            <td>
                Nek
            </td>
            <td>
                Italienische Musik 
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Lay Down Sally
            </td>
            <td>
                Eric Clapton
            </td>
            <td>
                1970er Rock 
            </td>
            <td>
                1977
            </td>
        </tr>
        <tr>
            <td>
                Learning To Fly
            </td>
            <td>
                Tom Petty
            </td>
            <td>
                1990er Rock-Pop 
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Lift Me Up
            </td>
            <td>
                Moby
            </td>
            <td>
                2000er Rock-Pop
            </td>
            <td>
                2005
            </td>
        </tr>
        <tr>
            <td>
                Love Is In The Air
            </td>
            <td>
                John Paul Young
            </td>
            <td>
                Englischer Schlager
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Macho Macho
            </td>
            <td>
                Rainhard Fendrich
            </td>
            <td>
                Deutschrock
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Mama Mia
            </td>
            <td>
                A-Teens
            </td>
            <td>
                2010er Rock-Pop
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Mein Ding
            </td>
            <td>
                Udo Lindenberg
            </td>
            <td>
                Deutschrock
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Mit 66 Jahren
            </td>
            <td>
                Udo J&#xfc;rgens
            </td>
            <td>
                Schlager
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Moonriver
            </td>
            <td>
                Louis Armstrong
            </td>
            <td>
                Englischer Schlager
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Moskau
            </td>
            <td>
                Dschinghis Khan
            </td>
            <td>
                Schlager
            </td>
            <td>
                1979
            </td>
        </tr>
        <tr>
            <td>
                Muss I Denn Muss I Denn Zum St&#xe4;dtele Hinaus
            </td>
            <td>
                Volkslied
            </td>
            <td>
                Volksmusik 
            </td>
            <td>
                1827
            </td>
        </tr>
        <tr>
            <td>
                My Way
            </td>
            <td>
                Calvin Harris
            </td>
            <td>
                2010er Rock-Pop
            </td>
            <td>
                2016
            </td>
        </tr>
        <tr>
            <td>
                Not The Loving Kind
            </td>
            <td>
                Twins
            </td>
            <td>
                1980er Rock-Pop 
            </td>
            <td>
                1983
            </td>
        </tr>
        <tr>
            <td>
                Ob-la-di Ob-la-da
            </td>
            <td>
                Beatles
            </td>
            <td>
                1960er Beat 
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Owner Of A Lonely Heart
            </td>
            <td>
                Yes
            </td>
            <td>
                1980er Rock-Pop 
            </td>
            <td>
                1983
            </td>
        </tr>
        <tr>
            <td>
                Polon&#xe4;se Blankenese
            </td>
            <td>
                Gottlieb Wendehals
            </td>
            <td>
                Schlager
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Promises
            </td>
            <td>
                Eric Clapton
            </td>
            <td>
                1970er Rock 
            </td>
            <td>
                1977
            </td>
        </tr>
        <tr>
            <td>
                Que Sera Mi Vida
            </td>
            <td>
                Gibson Brothers
            </td>
            <td>
                1980er Rock-Pop 
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Reality
            </td>
            <td>
                Lost Frequencies feat. Janieck Devy
            </td>
            <td>
                2010er Rock-Pop
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Rosamunde
            </td>
            <td>
                Volkslied
            </td>
            <td>
                Volksmusik 
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Rote Lippen Soll Man K&#xfc;ssen
            </td>
            <td>
                Cliff Richard
            </td>
            <td>
                Schlager
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Run To You
            </td>
            <td>
                Bryan Adams
            </td>
            <td>
                1980er Rock-Pop 
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Sacrifice
            </td>
            <td>
                Elton John
            </td>
            <td>
                1980er Rock-Pop 
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                San Francisco
            </td>
            <td>
                Scott Mc Kenzie
            </td>
            <td>
                1960er Beat 
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Ship Of Fools
            </td>
            <td>
                Erasure
            </td>
            <td>
                1980er Rock-Pop 
            </td>
            <td>
                1988
            </td>
        </tr>
        <tr>
            <td>
                Smalltown Boy
            </td>
            <td>
                Bronski Beat
            </td>
            <td>
                1980er Rock-Pop 
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Solsbury Hill
            </td>
            <td>
                Peter Gabriel
            </td>
            <td>
                1970er Rock 
            </td>
            <td>
                1977
            </td>
        </tr>
        <tr>
            <td>
                Something Got Me Started
            </td>
            <td>
                Simply Red
            </td>
            <td>
                2000er Rock-Pop
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Square Rooms
            </td>
            <td>
                Al Corley
            </td>
            <td>
                1980er Rock-Pop 
            </td>
            <td>
                1984
            </td>
        </tr>
        <tr>
            <td>
                Strangers In The Night
            </td>
            <td>
                Frank Sinatra
            </td>
            <td>
                Englischer Schlager
            </td>
            <td>
                1966
            </td>
        </tr>
        <tr>
            <td>
                Sultans Of Swing
            </td>
            <td>
                Dire Straits
            </td>
            <td>
                1980er Rock-Pop 
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Take It Easy
            </td>
            <td>
                Eagles
            </td>
            <td>
                1970er Rock 
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Tausendmal Du
            </td>
            <td>
                M&#xfc;nchener Freiheit
            </td>
            <td>
                Deutschrock
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                The Way To Your Heart
            </td>
            <td>
                Soulsister
            </td>
            <td>
                1980er Rock-Pop 
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                There She Goes
            </td>
            <td>
                La&#x27;s
            </td>
            <td>
                1980er Rock-Pop 
            </td>
            <td>
                1988
            </td>
        </tr>
        <tr>
            <td>
                Verdamp Lang Her
            </td>
            <td>
                BAP
            </td>
            <td>
                Deutschrock
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Verlieben Verloren Vergessen Verzeih&#x27;n
            </td>
            <td>
                Wolfgang Petry
            </td>
            <td>
                Schlager
            </td>
            <td>
                1992
            </td>
        </tr>
        <tr>
            <td>
                Wahnsinn
            </td>
            <td>
                Wolfgang Petry
            </td>
            <td>
                Schlager
            </td>
            <td>
                1983
            </td>
        </tr>
        <tr>
            <td>
                Walking On Sunshine
            </td>
            <td>
                Katrina 
            </td>
            <td>
                1980er Rock-Pop 
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Westerland
            </td>
            <td>
                &#xc4;rzte
            </td>
            <td>
                Deutschrock
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                What A Wonderful World
            </td>
            <td>
                Louis Armstrong
            </td>
            <td>
                Englischer Schlager
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                What Shall We Do With The Drunken Sailor
            </td>
            <td>
                Volkslied
            </td>
            <td>
                Volksmusik 
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Whatever Will Be Will Be (Que Sera Sera)
            </td>
            <td>
                Doris Day
            </td>
            <td>
                1950er Rock &#x27;n&#x27; Roll 
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Who Can It Be Now
            </td>
            <td>
                Men At Work
            </td>
            <td>
                1980er Rock-Pop 
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Wilde Kirschen
            </td>
            <td>
                Udo J&#xfc;rgens
            </td>
            <td>
                Schlager
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Willenlos
            </td>
            <td>
                Marius M&#xfc;ller-Westernhagen
            </td>
            <td>
                Deutschrock
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Wolke 4
            </td>
            <td>
                Philipp Dittberner 
            </td>
            <td>
                2010er Rock-Pop
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Wonderful Tonight
            </td>
            <td>
                Eric Clapton
            </td>
            <td>
                Englischer Schlager
            </td>
            <td>
                1977
            </td>
        </tr>
        <tr>
            <td>
                Y.M.C.A.
            </td>
            <td>
                Village People
            </td>
            <td>
                1970er Rock 
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                You&#x27;ll Be In My Heart
            </td>
            <td>
                Phil Collins
            </td>
            <td>
                2000er Rock-Pop
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Zorba&#x27;s Dance
            </td>
            <td>
                Mikis Theodorakis
            </td>
            <td>
                Volksmusik 
            </td>
            <td>
                1964
            </td>
        </tr>
        <tr>
            <td>
                &#xdc;ber Den Wolken
            </td>
            <td>
                Dieter-Thomas Kuhn
            </td>
            <td>
                Schlager
            </td>
            <td>
                1997
            </td>
        </tr>
    </tbody>
</table>
