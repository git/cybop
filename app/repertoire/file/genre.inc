<main>
    <div>
        <h2>
            1950er Rock &#x27;n&#x27; Roll 
        </h2>
        <ol>
            <li>
                Doris Day - Whatever Will Be Will Be (Que Sera Sera)
            </li>
            <li>
                Elvis Presley - Always On My Mind
            </li>
            <li>
                Elvis Presley - Are You Lonesome Tonight
            </li>
            <li>
                Mavericks - Foolish Heart
            </li>
        </ol>
    </div>
    <div>
        <h2>
            1960er Beat 
        </h2>
        <ol>
            <li>
                Beatles - Ob-la-di Ob-la-da
            </li>
            <li>
                Bob Dylan - Knockin&#x27; On Heaven&#x27;s Door
            </li>
            <li>
                Scott Mc Kenzie - San Francisco
            </li>
        </ol>
    </div>
    <div>
        <h2>
            1970er Rock 
        </h2>
        <ol>
            <li>
                C.C.R. - Bad Moon Rising
            </li>
            <li>
                C.C.R. - Hey Tonight
            </li>
            <li>
                Eagles - Take It Easy
            </li>
            <li>
                Eric Clapton - Lay Down Sally
            </li>
            <li>
                Eric Clapton - Promises
            </li>
            <li>
                Peter Gabriel - Solsbury Hill
            </li>
            <li>
                Village People - Y.M.C.A.
            </li>
        </ol>
    </div>
    <div>
        <h2>
            1980er Rock-Pop 
        </h2>
        <ol>
            <li>
                Al Corley - Square Rooms
            </li>
            <li>
                Alan Parsons Project - Eye In The Sky
            </li>
            <li>
                Black - Everything&#x27;s Coming Up Roses
            </li>
            <li>
                Bronski Beat - Smalltown Boy
            </li>
            <li>
                Bruce Springsteen - I&#x27;m On Fire
            </li>
            <li>
                Bryan Adams - Run To You
            </li>
            <li>
                Depeche Mode - Enjoy The Silence
            </li>
            <li>
                Dire Straits - Sultans Of Swing
            </li>
            <li>
                Elton John - Sacrifice
            </li>
            <li>
                Erasure - Ship Of Fools
            </li>
            <li>
                Fancy - Flames Of Love
            </li>
            <li>
                Fiction Factory - Feels Like Heaven
            </li>
            <li>
                Gibson Brothers - Que Sera Mi Vida
            </li>
            <li>
                Katrina  - Walking On Sunshine
            </li>
            <li>
                Kiss - I Was Made For Loving You
            </li>
            <li>
                La&#x27;s - There She Goes
            </li>
            <li>
                Men At Work - Who Can It Be Now
            </li>
            <li>
                Nick Straker Band - A Walk In The Park
            </li>
            <li>
                Police - Every Breath You Take
            </li>
            <li>
                Soulsister - The Way To Your Heart
            </li>
            <li>
                Toto - Africa
            </li>
            <li>
                Toto - Child&#x27;s Anthem
            </li>
            <li>
                Twins - Not The Loving Kind
            </li>
            <li>
                Ultravox - Dancing With Tears In My Eyes
            </li>
            <li>
                Yes - Owner Of A Lonely Heart
            </li>
        </ol>
    </div>
    <div>
        <h2>
            1990er Rock-Pop 
        </h2>
        <ol>
            <li>
                Londonbeat - I&#x27;ve Been Thinking About You
            </li>
            <li>
                Nick Kamen - I Promised Myself
            </li>
            <li>
                Roy Orbison - I Drove All Night
            </li>
            <li>
                Tom Petty - I Won&#x27;t Back Down
            </li>
            <li>
                Tom Petty - Learning To Fly
            </li>
        </ol>
    </div>
    <div>
        <h2>
            2000er Rock-Pop
        </h2>
        <ol>
            <li>
                a-ha - Foot Of The Mountain
            </li>
            <li>
                Alcazar - Crying At The Discotheque
            </li>
            <li>
                Fugees - Killing Me Softly
            </li>
            <li>
                Moby - Lift Me Up
            </li>
            <li>
                Phil Collins - You&#x27;ll Be In My Heart
            </li>
            <li>
                Robbie Williams - Angels
            </li>
            <li>
                Simply Red - Something Got Me Started
            </li>
            <li>
                Sportfreunde Stiller - Ein Kompliment
            </li>
        </ol>
    </div>
    <div>
        <h2>
            2010er Rock-Pop
        </h2>
        <ol>
            <li>
                A-Teens - Mama Mia
            </li>
            <li>
                Andreas Bourani - Alles Nur In Meinem Kopf
            </li>
            <li>
                Calvin Harris - Feel So Close
            </li>
            <li>
                Calvin Harris - My Way
            </li>
            <li>
                Lost Frequencies feat. Janieck Devy - Reality
            </li>
            <li>
                Philipp Dittberner  - Wolke 4
            </li>
            <li>
                Sheppard - Geronimo
            </li>
            <li>
                Sunrise Avenue - Hollywood Hills
            </li>
        </ol>
    </div>
    <div>
        <h2>
            Country Music
        </h2>
        <ol>
            <li>
                John Denver - Country Roads
            </li>
            <li>
                Ricky Nelson - Hello Mary Lou
            </li>
        </ol>
    </div>
    <div>
        <h2>
            Deutschrock
        </h2>
        <ol>
            <li>
                Achim Reichel - Aloha Heja He
            </li>
            <li>
                &#xc4;rzte - Westerland
            </li>
            <li>
                BAP - Verdamp Lang Her
            </li>
            <li>
                Heinz-Rudolf Kunze - Dein Ist Mein Ganzes Herz
            </li>
            <li>
                Klaus Lage Band - 1000 und 1 Nacht (Zoom!)
            </li>
            <li>
                Marius M&#xfc;ller-Westernhagen - Willenlos
            </li>
            <li>
                M&#xfc;nchener Freiheit - Herzschlag Ist Der Takt
            </li>
            <li>
                M&#xfc;nchener Freiheit - Tausendmal Du
            </li>
            <li>
                Peter Maffay - Eiszeit
            </li>
            <li>
                Purple Schulz - Du Hast Mir Gerade Noch Gefehlt
            </li>
            <li>
                Purple Schulz - Kleine Seen
            </li>
            <li>
                Rainhard Fendrich - Macho Macho
            </li>
            <li>
                Udo Lindenberg - Mein Ding
            </li>
        </ol>
    </div>
    <div>
        <h2>
            Englischer Schlager
        </h2>
        <ol>
            <li>
                Barry Manilow - Copacabana
            </li>
            <li>
                Cliff Richard - Dreamin&#x27;
            </li>
            <li>
                Eric Clapton - Wonderful Tonight
            </li>
            <li>
                Frank Sinatra - Strangers In The Night
            </li>
            <li>
                John Paul Young - Love Is In The Air
            </li>
            <li>
                Louis Armstrong - Moonriver
            </li>
            <li>
                Louis Armstrong - What A Wonderful World
            </li>
        </ol>
    </div>
    <div>
        <h2>
            Italienische Musik 
        </h2>
        <ol>
            <li>
                Nek - Laura Non C&#x27;e
            </li>
        </ol>
    </div>
    <div>
        <h2>
            Neue Deutsche Welle (NDW)
        </h2>
        <ol>
            <li>
                Joachim Witt - Goldener Reiter
            </li>
            <li>
                Steinwolke - Katharine Katharine
            </li>
        </ol>
    </div>
    <div>
        <h2>
            Ostrock (DDR)
        </h2>
        <ol>
            <li>
                Karat - Jede Stunde
            </li>
            <li>
                Keimzeit - Kling Klang
            </li>
            <li>
                Klaus-Renft-Combo - Apfeltraum
            </li>
        </ol>
    </div>
    <div>
        <h2>
            Schlager
        </h2>
        <ol>
            <li>
                Cliff Richard - Rote Lippen Soll Man K&#xfc;ssen
            </li>
            <li>
                Costa Cordalis - Anita
            </li>
            <li>
                Dieter-Thomas Kuhn - &#xdc;ber Den Wolken
            </li>
            <li>
                DJ &#xd6;tzi  - Ein Stern (Der Deinen Namen tr&#xe4;gt)
            </li>
            <li>
                Dschinghis Khan - Moskau
            </li>
            <li>
                Gottlieb Wendehals - Polon&#xe4;se Blankenese
            </li>
            <li>
                Howard Carpendale - Hello Again
            </li>
            <li>
                Peter Alexander - Die Kleine Kneipe
            </li>
            <li>
                Peter Kraus - Hello Mary Lou
            </li>
            <li>
                Rudi Schuricke - Capri Fischer
            </li>
            <li>
                Udo J&#xfc;rgens - Griechischer Wein
            </li>
            <li>
                Udo J&#xfc;rgens - Ich War Noch Niemals In New York
            </li>
            <li>
                Udo J&#xfc;rgens - Mit 66 Jahren
            </li>
            <li>
                Udo J&#xfc;rgens - Wilde Kirschen
            </li>
            <li>
                Wolfgang Petry - Verlieben Verloren Vergessen Verzeih&#x27;n
            </li>
            <li>
                Wolfgang Petry - Wahnsinn
            </li>
        </ol>
    </div>
    <div>
        <h2>
            Spanischer Gitarren-Rock
        </h2>
        <ol>
            <li>
                Heroes del Silencio - Entre Dos Tierras
            </li>
        </ol>
    </div>
    <div>
        <h2>
            Volksmusik 
        </h2>
        <ol>
            <li>
                DJ &#xd6;tzi - Anton Aus Tirol
            </li>
            <li>
                Hans Albers - Auf Der Reeperbahn Nachts Um Halb Eins
            </li>
            <li>
                Mikis Theodorakis - Zorba&#x27;s Dance
            </li>
            <li>
                Paul Kuhn - Es Gibt Kein Bier Auf Hawaii
            </li>
            <li>
                Volkslied - Hoch Auf Dem Gelben Wagen
            </li>
            <li>
                Volkslied - Kufsteiner Lied
            </li>
            <li>
                Volkslied - Muss I Denn Muss I Denn Zum St&#xe4;dtele Hinaus
            </li>
            <li>
                Volkslied - Rosamunde
            </li>
            <li>
                Volkslied - What Shall We Do With The Drunken Sailor
            </li>
        </ol>
    </div>
</main>
