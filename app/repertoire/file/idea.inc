<main>
    <div>
        <h2>
            1950er Rock &#x27;n&#x27; Roll 
        </h2>
        <ol>
            <li>
                Elvis Presley - Devil In Disguise
            </li>
        </ol>
    </div>
    <div>
        <h2>
            1960er Beat 
        </h2>
        <ol>
            <li>
                Birds - Mr. Tambourine Man
            </li>
            <li>
                Mamas  - Monday Monday
            </li>
        </ol>
    </div>
    <div>
        <h2>
            1970er Rock 
        </h2>
        <ol>
            <li>
                C.C.R. - Have You Ever Seen The Rain
            </li>
            <li>
                C.C.R. - Proud Mary
            </li>
            <li>
                Doors - Love Street
            </li>
            <li>
                Dr. Hook - When You&#x27;re In Love With A Beautiful Woman
            </li>
            <li>
                George McCrae - Rock Your Baby
            </li>
            <li>
                Lobo - I&#x27;d Love You To Want Me
            </li>
            <li>
                Smokie - I&#x27;ll Meet You At Midnight
            </li>
            <li>
                Tokens - The Lion Sleeps Tonight
            </li>
        </ol>
    </div>
    <div>
        <h2>
            1980er Rock-Pop 
        </h2>
        <ol>
            <li>
                A Flock Of Seagulls - I Ran So Far Away
            </li>
            <li>
                A Flock Of Seagulls - The More You Live The More You Love
            </li>
            <li>
                Alan Parsons Project - Don&#x27;t Answer Me
            </li>
            <li>
                Asia - Heat Of The Moment
            </li>
            <li>
                Bad Boys Blue - A World Without You
            </li>
            <li>
                Barclay James Harvest - Life Is For Living
            </li>
            <li>
                Bryan Adams - Somebody
            </li>
            <li>
                Bryan Adams - Summer Of &#x27;69
            </li>
            <li>
                Bryan Ferry  - Jealous Guy
            </li>
            <li>
                Communards - Disenchanted
            </li>
            <li>
                E.L.O. - Calling America
            </li>
            <li>
                Europe - The Final Countdown
            </li>
            <li>
                Johnny Nash - Rock Me Baby
            </li>
            <li>
                Jon  - I&#x27;ll Find My Way Home
            </li>
            <li>
                Joy - Touch By Touch
            </li>
            <li>
                Limahl - Neverending Story
            </li>
            <li>
                Men at Work - Down Under
            </li>
            <li>
                Nazareth - Dream On
            </li>
            <li>
                Pet Shop Boys - It&#x27;s A Sin
            </li>
            <li>
                Police - Message In A Bottle
            </li>
            <li>
                Queen - I Want To Break Free
            </li>
            <li>
                Saragossa Band - Agadou
            </li>
            <li>
                Scorpions - White Dove
            </li>
            <li>
                Shakin&#x27; Stevens - You Drive Me Crazy
            </li>
            <li>
                Status Quo - Rockin&#x27; All Over The World
            </li>
            <li>
                Stranglers - Skin Deep
            </li>
            <li>
                Tears for Fears - Everybody Wants To Rule The World
            </li>
            <li>
                U2 - I Still Haven&#x27;t Found What I&#x27;m Looking For
            </li>
            <li>
                U2 - Pride (In The Name Of Love)
            </li>
            <li>
                U2 - Sunday Bloody Sunday
            </li>
            <li>
                Ultravox - Hymn
            </li>
        </ol>
    </div>
    <div>
        <h2>
            1990er Rock-Pop 
        </h2>
        <ol>
            <li>
                Chyp Notic - I Can&#x27;t Get Enough
            </li>
            <li>
                Cure - Friday I&#x27;m In Love
            </li>
            <li>
                Lightening Seeds - Life Of Riley
            </li>
            <li>
                Mike  - Another Cup Of Coffee
            </li>
            <li>
                No Mercy - Where Do You Go
            </li>
            <li>
                OMD - Pandora&#x27;s Box
            </li>
            <li>
                R.E.M. - The One I Love
            </li>
            <li>
                Sparks - When Do I Get To Sing My Way
            </li>
            <li>
                Tom Petty - Into The Great Wide Open
            </li>
            <li>
                Wolfsheim - The Sparrows and the Nightingales
            </li>
        </ol>
    </div>
    <div>
        <h2>
            2000er Rock-Pop
        </h2>
        <ol>
            <li>
                Coldplay - Clocks
            </li>
            <li>
                Coldplay - Speed Of Sound
            </li>
            <li>
                Coldplay - Talk
            </li>
            <li>
                Coldplay - The Hardest Part
            </li>
            <li>
                Coldplay - Viva La Vida
            </li>
            <li>
                Killers - Human
            </li>
            <li>
                Puhdys - Hey Wir Wollen Die Eisb&#xe4;ren Seh&#x27;n
            </li>
            <li>
                Rosenstolz - Wir Sind Am Leben
            </li>
            <li>
                Take That - Patience
            </li>
        </ol>
    </div>
    <div>
        <h2>
            2010er Rock-Pop
        </h2>
        <ol>
            <li>
                Daft Punk - Get Lucky
            </li>
            <li>
                Ed Sheeran - Perfect
            </li>
        </ol>
    </div>
    <div>
        <h2>
            Country Music
        </h2>
        <ol>
            <li>
                Texas Lightning - No No Never
            </li>
        </ol>
    </div>
    <div>
        <h2>
            Deutschrock
        </h2>
        <ol>
        </ol>
    </div>
    <div>
        <h2>
            Englischer Schlager
        </h2>
        <ol>
            <li>
                Eric Clapton - Tears In Heaven
            </li>
            <li>
                George Baker Selection - Paloma Blanca
            </li>
            <li>
                Peter Kent - It&#x27;s A Real Good Feeling
            </li>
            <li>
                Pussycat - Mississippi
            </li>
            <li>
                Pussycat - Teenage Queenie
            </li>
            <li>
                Secret Service - Ten o&#x27;clock Postman
            </li>
        </ol>
    </div>
    <div>
        <h2>
            Italienische Musik 
        </h2>
        <ol>
            <li>
                Adriano Celentano - Azzuro
            </li>
            <li>
                Ricchi e Poveri - Acapulco
            </li>
            <li>
                Ricchi e Poveri - Sara Perche Ti Amo
            </li>
        </ol>
    </div>
    <div>
        <h2>
            Neue Deutsche Welle (NDW)
        </h2>
        <ol>
            <li>
                Hubert Kah - Sternenhimmel
            </li>
            <li>
                Peter Schilling - Major Tom
            </li>
        </ol>
    </div>
    <div>
        <h2>
            Ostrock (DDR)
        </h2>
        <ol>
            <li>
                City - Am Fenster
            </li>
            <li>
                Karat - Gewitterregen
            </li>
            <li>
                Puhdys - Alt Wie Ein Baum
            </li>
            <li>
                Puhdys - Wenn Tr&#xe4;ume Sterben
            </li>
            <li>
                Ute Freudenberg - Jugendliebe
            </li>
            <li>
                Wolfgang Ziegler - Verdammt
            </li>
        </ol>
    </div>
    <div>
        <h2>
            Schlager
        </h2>
        <ol>
            <li>
                Andrea Berg - Du Hast Mich Tausendmal Belogen
            </li>
            <li>
                Flippers - Lotosblume
            </li>
            <li>
                Frank Zander - Ententanz (Ja Wenn Wir Alle Englein W&#xe4;ren)
            </li>
            <li>
                Helene Fischer - Atemlos (Durch Die Nacht)
            </li>
            <li>
                Nico Haak - Schmidtchen Schleicher
            </li>
            <li>
                Nina  - Fahrende Musikanten Das Sind Wir
            </li>
            <li>
                Peter Cornelius - Du Entschuldige I Kenn&#x27; Di
            </li>
            <li>
                Roger Whittaker - Albany
            </li>
            <li>
                Shorts - Comment Ca Va
            </li>
            <li>
                Tina York - Wir Lassen Uns Das Singen Nicht Verbieten
            </li>
            <li>
                Udo J&#xfc;rgens - Aber Bitte Mit Sahne
            </li>
            <li>
                Wildecker Herzbuben - Hallo Frau Nachbarin
            </li>
        </ol>
    </div>
    <div>
        <h2>
            Spanischer Gitarren-Rock
        </h2>
        <ol>
        </ol>
    </div>
    <div>
        <h2>
            Volksmusik 
        </h2>
        <ol>
            <li>
                Franz Lang - Appenzeller Jodler
            </li>
            <li>
                Gitti  - Aus B&#xf6;hmen Kommt Die Musik
            </li>
            <li>
                Gitti  - Solange Die Kapelle Spielt
            </li>
            <li>
                Herbert Roth - Probier&#x27;s Mal Mit Jodeln
            </li>
            <li>
                Herbert Roth - Rennsteiglied
            </li>
            <li>
                J&#xfc;rgen Hart - Sing Mei Sachse Sing
            </li>
            <li>
                Volkslied - Das Lieben Bringt Gro&#xdf; Freud
            </li>
            <li>
                Volkslied - Die Gedanken Sind Frei
            </li>
            <li>
                Volkslied - Es Wollt&#x27; Ein M&#xe4;gdlein Fr&#xfc;h Aufsteh&#x27;n
            </li>
            <li>
                Volkslied - Es Zogen Auf Sonnigen Wegen
            </li>
            <li>
                Volkslied - Heute Wollen Wir Das R&#xe4;nzlein Schn&#xfc;ren
            </li>
            <li>
                Volkslied - Im Fr&#xfc;htau Zu Berge
            </li>
            <li>
                Roland Steinel - Liechtensteiner Polka
            </li>
            <li>
                Volkslied - Mein Vater War Ein Wandersmann (Der fr&#xf6;hliche Wanderer)
            </li>
            <li>
                Volkslied - Schneewalzer (Herbert Roth)
            </li>
            <li>
                Volkslied - Wem Gott Will Rechte Gunst Erweisen
            </li>
            <li>
                Volkslied - Wenn Wir Erklimmen (Bergvagabunden)
            </li>
            <li>
                Volkslied - Wir Lieben Die St&#xfc;rme Die Brausenden Wogen
            </li>
        </ol>
    </div>
</main>
