<!--
    This application demonstrates the server side of a socket communication.

    CAUTION! The reserved service ports 0..1023 can sometimes only be
    used when running the cybol application as "root" user (Administrator).

    The "nmap" tool may be used to find out about active service ports, e.g.:
    nmap localhost
-->

<node>

    <!-- Creation -->

    <node name="create_fileid" channel="inline" format="memorise/create" model="">
        <node name="name" channel="inline" format="text/plain" model="id"/>
        <node name="format" channel="inline" format="meta/format" model="number/integer"/>
    </node>

    <!-- Initialisation -->

    <node name="open_file" channel="inline" format="dispatch/open" model="">
        <node name="channel" channel="inline" format="meta/channel" model="file"/>
        <node name="identification" channel="inline" format="text/cybol-path" model=".id"/>
        <node name="device" channel="inline" format="text/plain" model="internet/socket_server/app.cybol"/>
    </node>
    <node name="initialise_app" channel="inline" format="communicate/receive" model="">
        <node name="channel" channel="inline" format="meta/channel" model="file"/>
        <node name="sender" channel="inline" format="text/cybol-path" model=".id"/>
        <node name="encoding" channel="inline" format="meta/encoding" model="utf-8"/>
        <node name="language" channel="inline" format="meta/language" model="text/cybol"/>
        <node name="format" channel="inline" format="meta/format" model="element/part"/>
        <node name="message" channel="inline" format="text/cybol-path" model="."/>
    </node>
    <node name="close_file" channel="inline" format="dispatch/close" model="">
        <node name="channel" channel="inline" format="meta/channel" model="file"/>
        <node name="identification" channel="inline" format="text/cybol-path" model=".id"/>
    </node>

    <node name="open_stdout" channel="inline" format="dispatch/open" model="">
        <node name="channel" channel="inline" format="meta/channel" model="terminal"/>
        <node name="device" channel="inline" format="text/plain" model="standard-output"/>
        <node name="identification" channel="inline" format="text/cybol-path" model=".stdout"/>
    </node>

    <!-- Reading -->

    <node name="print_read_data" channel="inline" format="communicate/send" model="">
        <node name="channel" channel="inline" format="meta/channel" model="terminal"/>
        <node name="receiver" channel="inline" format="text/cybol-path" model=".stdout"/>
        <node name="language" channel="inline" format="meta/language" model="message/tui"/>
        <node name="format" channel="inline" format="meta/format" model="text/plain"/>
        <node name="message" channel="inline" format="text/plain" model="Read data from file."/>
    </node>
    <node name="open_file" channel="inline" format="dispatch/open" model="">
        <node name="channel" channel="inline" format="meta/channel" model="file"/>
        <node name="identification" channel="inline" format="text/cybol-path" model=".datafile"/>
        <node name="device" channel="inline" format="text/plain" model="internet/socket_server/binary.dat"/>
    </node>
    <node name="read_data" channel="inline" format="communicate/receive" model="">
        <node name="channel" channel="inline" format="meta/channel" model="file"/>
        <node name="sender" channel="inline" format="text/cybol-path" model=".datafile"/>
        <node name="language" channel="inline" format="meta/language" model="message/binary"/>
        <node name="message" channel="inline" format="text/cybol-path" model=".data"/>
    </node>
    <node name="close_file" channel="inline" format="dispatch/close" model="">
        <node name="channel" channel="inline" format="meta/channel" model="file"/>
        <node name="identification" channel="inline" format="text/cybol-path" model=".datafile"/>
    </node>

    <!-- Startup -->

    <node name="print_message" channel="inline" format="communicate/send" model="">
        <node name="channel" channel="inline" format="meta/channel" model="terminal"/>
        <node name="receiver" channel="inline" format="text/cybol-path" model=".stdout"/>
        <node name="language" channel="inline" format="meta/language" model="message/tui"/>
        <node name="format" channel="inline" format="meta/format" model="text/plain"/>
        <node name="message" channel="inline" format="text/plain" model="Startup socket."/>
    </node>

<!--
    <node name="startup_socket" channel="inline" format="maintain/startup" model="">
        <node name="channel" channel="inline" format="meta/channel" model="socket"/>
        <node name="namespace" channel="inline" format="text/plain" model="local"/>
        <node name="style" channel="inline" format="text/plain" model="datagram"/>
        <node name="protocol" channel="inline" format="text/plain" model="udp"/>
        <node name="device" channel="inline" format="text/plain" model="cyboi.socket"/>
        <node name="connexions" channel="inline" format="number/integer" model="1"/>
        <node name="timeout" channel="inline" format="number/integer" model="5"/>
    </node>
-->
    <node name="startup_socket" channel="inline" format="maintain/startup" model="">
        <node name="channel" channel="inline" format="meta/channel" model="socket"/>
        <node name="port" channel="inline" format="number/integer" model="1971"/>
        <node name="namespace" channel="inline" format="text/plain" model="ipv4"/>
        <node name="style" channel="inline" format="text/plain" model="stream"/>
        <node name="protocol" channel="inline" format="text/plain" model="tcp"/>
        <node name="device" channel="inline" format="text/plain" model="127.0.0.1"/>
        <node name="connexions" channel="inline" format="number/integer" model="10"/>
        <node name="timeout" channel="inline" format="number/integer" model="5"/>
    </node>

    <!-- Waiting -->

    <node name="print_message" channel="inline" format="communicate/send" model="">
        <node name="channel" channel="inline" format="meta/channel" model="terminal"/>
        <node name="receiver" channel="inline" format="text/cybol-path" model=".stdout"/>
        <node name="language" channel="inline" format="meta/language" model="message/tui"/>
        <node name="format" channel="inline" format="meta/format" model="text/plain"/>
        <node name="message" channel="inline" format="text/plain" model="Wait for client requests."/>
    </node>

    <node name="enable_requests" channel="inline" format="activate/enable" model="">
        <node name="channel" channel="inline" format="meta/channel" model="socket"/>
        <node name="port" channel="inline" format="number/integer" model="1971"/>
        <node name="handler" channel="inline" format="text/cybol-path" model=".logic.handle_enable"/>
    </node>

</node>
