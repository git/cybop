<node>

    <!-- Identification -->

    <!-- Get client belonging to the handler that was placed into the interrupt pipe. -->
    <node name="get_client_id" channel="inline" format="communicate/identify" model="">
        <node name="identification" channel="inline" format="text/cybol-path" model=".client_socket"/>
    </node>

    <node name="print_accept" channel="inline" format="communicate/send" model="">
        <node name="channel" channel="inline" format="meta/channel" model="terminal"/>
        <node name="receiver" channel="inline" format="text/cybol-path" model=".stdout"/>
        <node name="language" channel="inline" format="meta/language" model="message/tui"/>
        <node name="format" channel="inline" format="meta/format" model="text/plain"/>
        <node name="message" channel="inline" format="text/plain" model="Receive client data on socket:"/>
    </node>
    <node name="print_socket" channel="inline" format="communicate/send" model="">
        <node name="channel" channel="inline" format="meta/channel" model="terminal"/>
        <node name="receiver" channel="inline" format="text/cybol-path" model=".stdout"/>
        <node name="language" channel="inline" format="meta/language" model="message/tui"/>
        <node name="format" channel="inline" format="meta/format" model="number/integer"/>
        <node name="message" channel="inline" format="text/cybol-path" model=".client_socket"/>
    </node>

    <!-- Reset -->

    <!-- CAUTION! If the request was not reset here, then the last valid previous request would be used again and again, neverendingly. -->
    <node name="reset_request_model" channel="inline" format="modify/empty" model="">
        <node name="destination" channel="inline" format="text/cybol-path" model=".request"/>
        <node name="destination_properties" channel="inline" format="logicvalue/boolean" model="false"/>
    </node>
    <node name="reset_request_properties" channel="inline" format="modify/empty" model="">
        <node name="destination" channel="inline" format="text/cybol-path" model=".request"/>
        <node name="destination_properties" channel="inline" format="logicvalue/boolean" model="true"/>
    </node>

    <!-- Reception -->

    <node name="receive_request" channel="inline" format="communicate/receive" model="">
        <node name="channel" channel="inline" format="meta/channel" model="socket"/>
        <node name="server" channel="inline" format="logicvalue/boolean" model="true"/>
        <node name="port" channel="inline" format="number/integer" model="1971"/>
        <node name="sender" channel="inline" format="text/cybol-path" model=".client_socket"/>
        <node name="language" channel="inline" format="meta/language" model="message/http-request"/>
        <node name="format" channel="inline" format="meta/format" model="element/part"/>
        <node name="message" channel="inline" format="text/cybol-path" model=".request"/>
        <!-- Read indirectly from internal buffer into which data have been written by the sensing thread. -->
        <node name="asynchronicity" channel="inline" format="logicvalue/boolean" model="true"/>
    </node>

    <!-- Determination -->

    <!-- CAUTION! Use "overwrite" here for initialisation and "append" only afterwards. -->
    <node name="overwrite_path" channel="inline" format="modify/overwrite" model="">
        <node name="destination" channel="inline" format="text/cybol-path" model=".path"/>
        <node name="source" channel="inline" format="text/plain" model="internet/www_server_static/files"/>
    </node>
    <node name="append_path" channel="inline" format="modify/append" model="">
        <node name="destination" channel="inline" format="text/cybol-path" model=".path"/>
        <node name="source" channel="inline" format="text/cybol-path" model=".request:uri:path"/>
    </node>

    <node name="print_path_label" channel="inline" format="communicate/send" model="">
        <node name="channel" channel="inline" format="meta/channel" model="terminal"/>
        <node name="receiver" channel="inline" format="text/cybol-path" model=".stdout"/>
        <node name="language" channel="inline" format="meta/language" model="message/tui"/>
        <node name="format" channel="inline" format="meta/format" model="text/plain"/>
        <node name="message" channel="inline" format="text/plain" model="Path:"/>
    </node>
    <node name="print_path" channel="inline" format="communicate/send" model="">
        <node name="channel" channel="inline" format="meta/channel" model="terminal"/>
        <node name="receiver" channel="inline" format="text/cybol-path" model=".stdout"/>
        <node name="language" channel="inline" format="meta/language" model="message/tui"/>
        <node name="format" channel="inline" format="meta/format" model="text/plain"/>
        <node name="message" channel="inline" format="text/cybol-path" model=".path"/>
    </node>

    <!-- Processing -->

    <node name="reset_file_model" channel="inline" format="modify/empty" model="">
        <node name="destination" channel="inline" format="text/cybol-path" model=".file"/>
        <node name="destination_properties" channel="inline" format="logicvalue/boolean" model="false"/>
    </node>

    <node name="open_file" channel="inline" format="dispatch/open" model="">
        <node name="channel" channel="inline" format="meta/channel" model="file"/>
        <node name="device" channel="inline" format="text/cybol-path" model=".path"/>
        <node name="identification" channel="inline" format="text/cybol-path" model=".id"/>
    </node>
    <node name="receive_file" channel="inline" format="communicate/receive" model="">
        <node name="channel" channel="inline" format="meta/channel" model="file"/>
        <node name="sender" channel="inline" format="text/cybol-path" model=".id"/>
        <node name="language" channel="inline" format="meta/language" model="message/binary"/>
        <node name="message" channel="inline" format="text/cybol-path" model=".file"/>
    </node>
    <node name="close_file" channel="inline" format="dispatch/close" model="">
        <node name="channel" channel="inline" format="meta/channel" model="file"/>
        <node name="identification" channel="inline" format="text/cybol-path" model=".id"/>
    </node>

    <!-- Testing -->

    <node name="open_file" channel="inline" format="dispatch/open" model="">
        <node name="channel" channel="inline" format="meta/channel" model="file"/>
        <node name="identification" channel="inline" format="text/cybol-path" model=".id"/>
        <node name="device" channel="inline" format="text/plain" model="internet/www_server_static/test_file.txt"/>
        <node name="mode" channel="inline" format="text/plain" model="write"/>
    </node>
    <node name="test_file" channel="inline" format="communicate/send" model="">
        <node name="channel" channel="inline" format="meta/channel" model="file"/>
        <node name="receiver" channel="inline" format="text/cybol-path" model=".id"/>
        <node name="language" channel="inline" format="meta/language" model="message/binary"/>
        <node name="message" channel="inline" format="text/cybol-path" model=".file"/>
    </node>
    <node name="close_file" channel="inline" format="dispatch/close" model="">
        <node name="channel" channel="inline" format="meta/channel" model="file"/>
        <node name="identification" channel="inline" format="text/cybol-path" model=".id"/>
    </node>

    <!-- Sending -->

    <node name="send_file" channel="inline" format="communicate/send" model="">
        <node name="channel" channel="inline" format="meta/channel" model="socket"/>
        <node name="server" channel="inline" format="logicvalue/boolean" model="true"/>
        <node name="port" channel="inline" format="number/integer" model="1971"/>
        <node name="receiver" channel="inline" format="text/cybol-path" model=".client_socket"/>
        <node name="language" channel="inline" format="meta/language" model="message/http-response"/>
        <node name="message" channel="inline" format="text/cybol-path" model=".file"/>
    </node>

</node>
