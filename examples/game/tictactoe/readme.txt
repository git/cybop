<!--
    Copyright (C) 1999-2018. Christian Heller.

    This knowledge model uses the Cybernetics Oriented Language (CYBOL).

    CYBOL is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License,
    or (at your option) any later version.

    CYBOL is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty
    of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CYBOL. If not, see <http://www.gnu.org/licenses/>.

    Cybernetics Oriented Programming (CYBOP) <http://www.cybop.org/>
    CYBOP Developers <cybop-developers@nongnu.org>

    @version CYBOP 0.20.0 2018-06-30
    @slogan Tic Tac Toe
    @author Sandra Rum, Stephan Griese
    @helpers Christian Heller
--> 
