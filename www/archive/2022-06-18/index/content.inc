<section class="container mt-5">
    <div class="row mt-5">
        <div class="col-sm-6 p-5">
            <img src="/logo.png" alt="CYBOP Logo" style="width: 80px;"/>
            <span style="margin-left: 20px; vertical-align: middle; font-size: 4rem; font-weight: bold; color: #5287ce;">CYBOP</span>
            <p class="mt-5" style="font-size: 2rem; font-weight: bold;">
                The final abstraction.<br/>
                For any kind of knowledge.
            </p>
            <p>
                Cybernetics Oriented Programming (CYBOP)
            </p>
        </div>
        <div class="col-sm-6 p-5">
            <img src="/index/abstraction.png" alt="CYBOP Abstraction Levels" class="img-fluid mx-auto d-block"/>
        </div>
    </div>
</section>

<section class="container">
    <div class="card shadow">
        <div class="card-body">
            <h1 class="text-center" style="font-weight: bold;">Open Source Tree-based Software Development</h1>
        </div>
    </div>
</section>

<section class="container">

    <div class="row mt-5">
        <div class="col-sm-6 p-5">
            <h2>Straightforward Use of Hierarchy</h2>
            <p>
                Get to know the simplicity of tree-based application development.
                No more class frameworks. No more patterns. Just one hierarchy.
                Quite comparable to web development, but usable for any kind
                of application, with arbitrary user interface.
            </p>
        </div>
        <div class="col-sm-6 p-5">
            <img src="/index/tree.png" alt="CYBOL Tree Structure" class="img-fluid mx-auto d-block"/>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-sm-6 p-5">
            <img src="/index/syntax.png" alt="Uniform CYBOL Syntax" class="img-fluid mx-auto d-block"/>
        </div>
        <div class="col-sm-6 p-5">
            <h2>Uniform Syntax for Values and Functions</h2>
            <p>
                Work with one consistent programming schema for data and workflows.
                The same applies to the control structures sequence, branch and loop.
                Operations are not limited, so this is a general purpose language
                (not domain-specific).
            </p>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-sm-6 p-5">
            <h2>Clear API following MIME Types</h2>
            <p>
                Use the well-defined CYBOL operations
                which are documented as API on our website.
                To its standard categories belong:
                calculate, communicate, compare and many more.
            </p>
        </div>
        <div class="col-sm-6 p-5">
            <img src="/index/api.png" alt="CYBOL API Documentation" class="img-fluid mx-auto d-block"/>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-sm-6 p-5">
            <img src="/index/communication.png" alt="Universal CYBOL Communication" class="img-fluid mx-auto d-block"/>
        </div>
        <div class="col-sm-6 p-5">
            <h2>Universal Communication via Channels</h2>
            <p>
                Exchange data seamlessly between various channels
                in a star-like manner using synchronous or asynchronous mode.
                For instance via RS-232, USB, VT100 pseudo terminal, XCB, socket.
            </p>
        </div>
    </div>

</section>
