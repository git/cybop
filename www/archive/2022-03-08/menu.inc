        <div id="navigation" width="200px">
            <h1>General</h1>
            <a href="/cybop/index.html">
                Home
            </a>
            <a href="http://savannah.nongnu.org/files/?group=cybop">
                Download
            </a>
            <a href="http://savannah.nongnu.org/mail/?group=cybop">
                Mailing Lists
            </a>
            <a href="/cybop/licence/index.html">
                Licence Issues
            </a>
            <a href="http://www.gnu.org/gnu/thegnuproject.html">
                Philosophy
            </a>
            <a href="/cybop/credits/index.html">
                Credits
            </a>
            <h1>Documentation</h1>
            <a href="/cybop/manual/index.html">
                Beginner Manual
            </a>
            <a href="/cybop/books/index.html">
                Specification Books
            </a>
            <a href="/cybop/presentations/index.html">
                Developer Presentations
            </a>
            <a href="/cybop/papers/index.html">
                Developer Papers
            </a>
            <h1>Development</h1>
            <a href="http://savannah.nongnu.org/bugs/?group=cybop">
                Bug Report
            </a>
            <a href="http://savannah.nongnu.org/svn/?group=cybop">
                Source Code
            </a>
            <a href="/cybop/plan/index.html">
                Project Plan
            </a>
            <a href="/cybop/help/index.html">
                How to help
            </a>
            <div id="information">
                <a href="http://savannah.nongnu.org/projects/cybop/">
                    Our project hosted on:<br/>
                    <img src="/cybop/sv-nongnu.png" alt="GNU Savannah Logo" vspace="2" width="124" height="32" border="1"/>
                </a>
                <a href="http://www.ffii.org/">
                    Fight Software Patents!<br/>
                    <img src="/cybop/patent_button.png" alt="Eurolinux Petition" vspace="2" border="1"/>
                </a>
                <a href="https://resmedicinae.sourceforge.io/">
                    Res Medicinae<br/>
                    <img src="/cybop/res_medicinae.png" alt="Res Medicinae" vspace="2" border="1"/>
                </a>
            </div>
        </div>
