<!--#include virtual="/top.shtml" -->

<section class="container mt-5">
    <h1>CYBOP 0.6.0</h1>
</section>

<section class="container">
    <h2>Project</h2>
    <ul>
        <li>Adapt Makefile to restructured source code directories</li>
    </ul>
</section>

<section class="container">
    <h2>CYBOI</h2>
    <ul>
        <li>Prepare terminal handling of <i>Textual User Interfaces</i> (TUI);
            (see <a href="http://zemljanka.sourceforge.net/cursed/">Zemljanka</a>
            and <a href="http://tvision.sourceforge.net/">TurboVision</a>)</li>
        <li>Debug many errors caused by NULL_POINTER checks
            which tested the variables instead of their dereferenced values<br/>
            wrong: if (a != NULL_POINTER)<br/>
            right: if (*a != NULL_POINTER)<br/>
            this caused knowledge models not to be created/ added
            to the knowledge root/ <i>whole</i> model</li>
        <li>Replace NULL_POINTER with &NULL_POINTER,
            where a local void** variable is initialised</li>
        <li>Remove all <i>const</i> keywords before static variables
            and before parameters in headers of procedures,
            for less code, improved readability and because the
            <i>const</i> keyword is considered useless now:
            <ol>
                <li>pointers that were handed over do mostly not
                    get manipulated directly anyway, only pointers
                    within the array pointed to</li>
                <li>arrays defined with = {} are constant anyway</li>
            </ol>
        </li>
        <li>Delete all character counts and replace them
            with just one count constant</li>
        <li>Change and unify all comparisons, so that
            their <i>if</i> expressions now compare for
            equality with <i>0</i> instead of <i>1</i></li>
        <li>Replace constant count variables with the
            already existing integer counts, to save
            redundant code and also some memory, e.g.:<br/>
            static const int COMMA_CHARACTER_COUNT_ARRAY[] = {1};<br/>
            static const int* COMMA_CHARACTER_COUNT = COMMA_CHARACTER_COUNT_ARRAY;<br/>
            replaced with:<br/>
            static const int* COMMA_CHARACTER_COUNT = NUMBER_1_INTEGER_ARRAY;</li>
        <li>Delete superfluous source files for primitives;
            only vectors are used now everywhere</li>
        <li>Rename <i>string</i> to <i>character vector</i></li>
        <li>Simplify source code by replacing
            <i>POINTER_NULL_POINTER</i>, <i>INTEGER_NULL_POINTER</i>,
            <i>FILE_NULL_POINTER</i> etc. in <i>all</i>
            source files with just <i>NULL_POINTER</i></li>
        <li>Replace <i>allocate_integer</i> and others
            with more general procedure <i>allocate</i></li>
        <li>Prepare <i>send_terminal</i> procedure for tui output</li>
        <li>Rename low-level <i>receive</i>/ <i>send</i> into
            <i>read</i>/ <i>write</i>, to better distinguish them from
            the high-level <i>receive</i>/ <i>send</i> CYBOL operations</li>
        <li>Rename low-level <i>create</i>/ <i>destroy</i> into
            <i>allocate</i>/ <i>deallocate</i>, to better distinguish them from
            the high-level <i>create</i>/ <i>destroy</i> CYBOL operations</li>
        <li>Rename file <i>set.c</i> to <i>copy.c</i></li>
        <li>Add knowledge memory and signal memory to internal memory;
            this HAS TO BE DONE, because when using threads,
            ONLY ONE parameter (the internal memory) can be
            handed over to these</li>
        <li>Hand over <i>internal memory</i>, <i>knowledge memory</i>
            and <i>signal memory</i> as parameters, instead of
            reading them every time from <i>internal_memory</i></li>
        <li>Rename <i>internals_memory</i> to <i>internal_memory</i></li>
        <li>Adapt some include paths and procedure names after restructuring</li>
        <li>Delete memory managers and put code into general <i>manager.c</i></li>
        <li>Write log messages to a file instead of to console</li>
        <li>Replace all American English (e.g. <i>lize</i> suffixes)
            with British (Commonwealth) English (e.g. <i>lise</i>)
            which is now consistently used throughout all source code</li>
        <li>Move directories <i>communicator</i>, <i>converter</i>
            and <i>translator</i> from parent directory
            <i>controller</i> to parent directory <i>memoriser</i></li>
        <li>Rename directory <i>computer</i> to <i>applicator</i></li>
        <li>Restructure source code directories in relation
            to <i>von Neumann</i> computer architecture,
            i.e. consisting of the parts:
            <ul>
                <li>memoriser (Memory, containing states)</li>
                <li>controller (Control Unit, coordinating flow)</li>
                <li>applicator (Arithmetic Logic Unit = ALU, containing logic)</li>
                <li>globals (additional, global constants and variables)</li>
                <li>tester (additional, for testing)</li>
            </ul>
        </li>
    </ul>
</section>

<section class="container">
    <h2>CYBOL</h2>
    <ul>
        <li>Add some abstraction- and structure constants</li>
    </ul>
</section>

<!--#include virtual="/bottom.shtml" -->
