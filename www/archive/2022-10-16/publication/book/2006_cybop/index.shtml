<!--#include virtual="/top.shtml" -->

<section class="container mt-5">
    <h1>CYBOP Book</h1>
</section>

<section class="container">
    <h2>Selling</h2>
    <div class="row">
        <div class="col-sm-6">
            <p>
                Cybernetics Oriented Programming (CYBOP)<br/>
                An Investigation on the Applicability<br/>
                of Inter-Disciplinary Concepts<br/>
                to Software System Development<br/>
                Christian Heller<br/>
                Paperback: 536 Pages<br/>
                Publisher: Tux Tax; 1st edition (January 19, 2007)<br/>
                Language: English<br/>
                License: GNU FDL<br/>
                ISBN-13: 978-3-9810898-0-6<br/>
                Dimensions: 16.8 x 24.0 x 3.2 cm | 6.61 x 9.45 x 1.26 inches<br/>
                Weight: 1100 g / 2.43 pounds<br/>
            </p>
            <p class="mt-4">
                <a href="https://www.amazon.de/Cybernetics-Oriented-Programming-Christian-Heller/dp/3981089804/sr=8-1/qid=1169666060/ref=sr_1_1/302-7558907-0519230?ie=UTF8&s=books">
                    <span class="badge rounded-pill p-3" style="font-size: 14pt;">Order Book via Amazon</span>
                </a>
            </p>
            <p class="mt-4">
                <a href="mailto:info@cybop.de">
                    <span class="badge rounded-pill p-3" style="font-size: 14pt;">Order Book via E-Mail</span>
                </a>
            </p>
        </div>
        <div class="col-sm-6">
            <img src="/publication/book/2006_cybop/photography.png" alt="CYBOP Book Photography" class="img-fluid mx-auto d-block" style="width: 300px;"/>
            <form method="get" action="http://books.google.com/books/p/1901145991646216">
                <p>Full text search in CYBOP book (using Google Books):</p>
                <input name="q" id="b_vertical_searchbox" size="31" maxlength="255" value="" type="text">
                <input name="btnG" value="Search" type="submit">
                <input name="hl" value="en" type="hidden"/>
                <!-- ISBN -->
                <input name="vid" value="ISBN9783981089806" type="hidden"/>
                <input name="ie" value="ISO-8859-1" type="hidden"/>
                <input name="oe" value="ISO-8859-1" type="hidden"/>
            </form>
        </div>
    </div>
</section>

<section class="container">
    <h2>Review</h2>
    <ul>
        <li><a href="/publication/book/2006_cybop/2007-11-30_-_gilbert_herschberger_-_cybop-review.shtml">2007-11-30: Gilbert Herschberger, Marietta, Georgia, USA</a></li>
        <li><a href="/publication/book/2006_cybop/2007-07-16_-_max_kleiner_-_cybop-review.shtml">2007-07-16: Max Kleiner, Bern, Schweiz [German language]</a></li>
    </ul>
</section>

<section class="container">
    <h2>Abstract</h2>
    <p>
        In today's society, information and knowledge increasingly gain in importance.
        Software as one form of knowledge abstraction plays an important role thereby.
        The main difficulty in creating software is to cross the abstraction gap between
        concepts of human thinking and the requirements of a machine-like representation.
    </p>
    <p>
        Conventional paradigms of software design have managed to increase their
        level of abstraction, but still exhibit quite a few weaknesses.
        This work compares and improves traditional concepts of software development
        through ideas taken from other sciences and phenomenons of nature,
        respectively - therefore its name: cybernetics-oriented.
    </p>
    <p>
        Three recommendations resulting from this inter-disciplinary approach are:
        (1) a strict separation of active system-control software from pure, passive knowledge;
        (2) the usage of a new schema for knowledge representation, which is based on a
        double-hierarchy modelling whole-part relationships and meta information in a combined manner;
        (3) a distinct treatment of knowledge models representing states from those containing logic.
    </p>
    <p>
        For representing knowledge according to the proposed schema, an XML-based language
        named CYBOL was defined and a corresponding interpreter called CYBOI developed.
        Despite its simplicity, CYBOL is able to describe knowledge completely.
        A Free-/ Open Source Software project called Res Medicinae was founded
        to proof the general operativeness of the CYBOP approach.
    </p>
    <p>
        CYBOP offers a new theory of programming which seems to be promising,
        since it not only eliminates deficiencies of existing paradigms,
        but prepares the way for more flexible, long-life application systems.
        Because of its easily understandable concept of hierarchy, domain experts are put
        in a position to, themselves, actively contribute to application development.
        The implementation phase found in classical software engineering processes becomes superfluous.
    </p>
</section>

<!--#include virtual="/bottom.shtml" -->
