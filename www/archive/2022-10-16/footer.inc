<!--
    CAUTION! A small gap is added here between content and footer.
    The margin CANNOT be used as it shows the color of the html body.
    The padding CANNOT be used as it shows the background of the footer.
    Therefore, the BORDER is set here as CSS STYLE and its COLOR to that of the content.
-->
<footer>

    <div class="container">

        <div class="row">

            <div class="col-sm-3 text-left">
                <h4 class="mb-4">Menu</h4>
                <ul class="list-unstyled">
                    <li class="pt-1">
                        <a href="/">Home</a>
                    </li>
                    <li class="pt-1">
                        <a href="/download/">Download</a>
                    </li>
                    <li class="pt-1">
                        <a href="/documentation/">Documentation</a>
                    </li>
                    <li class="pt-1">
                        <a href="/development/">Development</a>
                    </li>
                    <li class="pt-1">
                        <a href="/publication/">Publication</a>
                    </li>
                    <li class="pt-1">
                        <a href="/support/">Support</a>
                    </li>
                </ul>
            </div>

            <div class="col-sm-4 text-center">
                <a href="/">
                    <img src="/logo.png" alt="CYBOP Project Logo" width="150px" class="img-fluid"/>
                </a>
                <p class="mt-2">
                    <h5>Efficient Programming by</h5>
                    <h5>Tree Schema Development</h5>
                </p>
            </div>

            <div class="col-sm-5 text-left">
                <h4 class="mb-4">Contact</h4>
                <p>
                    Mailing List: <a href="mailto:cybop-developers@nongnu.org">cybop-developers@nongnu.org</a><br/>
                    Project Administrator: <a href="mailto:christian.heller@cybop.org">christian.heller@cybop.org</a><br/>
                </p>
                <p>
                    Web: <a href="http://www.cybop.org/">http://www.cybop.org/</a><br/>
                    Portal: <a href="https://savannah.nongnu.org/projects/">https://savannah.nongnu.org/projects/</a><br/>
                </p>
            </div>

        </div>

        <div class="row mt-4">
            <div class="col-sm-12 text-left">
                <p>
                    Copyright &copy; 1999-2022.<br/>
                    Except where otherwise noted, content on this site is licensed under a Creative Commons Attribution 4.0 International license.<br/>
                    Attribution-NoDerivatives 4.0 International (CC BY-ND 4.0).
                </p>
                <p>
                    Last Update: 2022-06-18
                </p>
            </div>
        </div>

    </div>

</footer>
