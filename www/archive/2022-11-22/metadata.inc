<!--
    Do NOT use the meta element with the content attribute set to Content-Language:
    <meta name="language" content="de"/>
    Declare the default language using language attributes rather than HTTP.
    Example:
    <html lang="de-DE">
    Further information:
    https://www.w3.org/International/techniques/authoring-html#textprocessing
    Declaring a language is important for accessibility applications (screen readers) and search engines.
-->
<meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<!-- Bootstrap: The above meta tags *must* come first in the head; any other head content must come *after* these tags -->

<meta name="date" content="2022-06-18T12:00:00+01:00"/>
<meta name="author" content="Christian Heller"/>

<!--
    Using the "ico" image format and file suffix as follows did NOT work:
    <link rel="icon" href="/favicon.ico" type="image/ico" sizes="16x16">
    Therefore, "png" is used here.
-->
<link rel="icon" href="/favicon.png" type="image/png" sizes="16x16"/>

<!-- Bootstrap core CSS -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"/>

<!-- Custom styles. CAUTION! Include AFTER bootstrap. Some standard values are changed inside. -->
<link rel="stylesheet" href="/cybop.css"/>

<!-- Bootstrap icons -->
<!--
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.3/font/bootstrap-icons.css"/>
-->

<!-- Bootstrap core JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
